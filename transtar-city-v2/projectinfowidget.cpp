﻿#include "projectinfowidget.h"
#include "ui_projectinfowidget.h"
#include <QMetaType>
#include <QPainter>
#include <QDebug>

projectInfoWidget::projectInfoWidget(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::projectInfoWidget)
{
    ui->setupUi(this);
    ui->verticalLayout->setSpacing(0);
    ui->verticalLayout->setMargin(0);
    this->setLayout(ui->verticalLayout);
    //qRegisterMetaType<projectInfoWidget>("projectInfoWidget");

    //ui->projectNameLabel->setStyleSheet(":hover{background-color:blue;}");
    //ui->projectPathLabel->setStyleSheet(":hover{background-color:blue;}");
    //setStyle(QStyle);
}

projectInfoWidget::~projectInfoWidget()
{
    delete ui;
}

void projectInfoWidget::mousePressEvent(QMouseEvent *event){
    if(event->button()==Qt::LeftButton){
        //直接打开项目，不用弹出文件选择框
    }
}

Project projectInfoWidget::getProject() const
{
    return project;
}

void projectInfoWidget::setProject(const Project &value)
{
    project = value;
}


void projectInfoWidget::setProjectShowOnLabel(){
    ui->projectNameLabel->setText(project.getProjectName());
    //ui->projectNameLabel->setTextFormat();
    ui->projectPathLabel->setText(project.getProjectPath());
}


