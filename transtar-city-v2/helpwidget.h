﻿#ifndef HELPWIDGET_H
#define HELPWIDGET_H

#include <QWidget>
#include <QtHelp>

namespace Ui {
class HelpWidget;
}

class HelpWidget : public QWidget
{
    Q_OBJECT

public:
    explicit HelpWidget(QWidget *parent = 0);
    ~HelpWidget();

private:
    Ui::HelpWidget *ui;
    void initHelpWidget();
};

#endif // HELPWIDGET_H
