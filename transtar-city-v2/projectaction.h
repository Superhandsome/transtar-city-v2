﻿#ifndef PROJECTACTION_H
#define PROJECTACTION_H

#include <QObject>
#include <QAction>
#include "project.h"

class ProjectAction : public QAction
{
    Q_OBJECT
public:
    ProjectAction(QWidget *parent=0);
    Project getProject() const;
    void setProject(const Project &value);
public slots:
    void openProject();
signals:
    void sendProject(Project project);
private:
    Project project;
};

#endif // PROJECTACTION_H
