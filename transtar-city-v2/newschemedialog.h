﻿#ifndef NEWSCHEMEDIALOG_H
#define NEWSCHEMEDIALOG_H

#include <QDialog>
#include "project.h"

namespace Ui {
class NewSchemeDialog;
}

class NewSchemeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewSchemeDialog(Project *project,QWidget *parent = 0);
    ~NewSchemeDialog();
//    bool getIsFirstCreate() const;
//    void setIsFirstCreate(bool value);

public slots:
    void setOkButtonIsEnable(QString s);
    void okButtonClicked();
    void cancelButtonClicked();
signals:
    void createSucceed();
private:
    Ui::NewSchemeDialog *ui;
    Project *project;   //此方案所属的project

    void initBasicSchemeBox();
    bool copyDirectoryFiles(QString fromPath,QString toPath);

    QString randomId();
};

#endif // NEWSCHEMEDIALOG_H
