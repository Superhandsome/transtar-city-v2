﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "project.h"
#include "newprojectdialog.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
public slots:
    void createProjectSlot();
    void openProjectSlot();
    void openProjectSlot_frame(Project project);
    void closeThis();
    void createNewSchemeDialog(Project project);
private:
    Ui::Widget *ui;
    void initNavigateListWidget();
    void initProjectListWidget();
    NewProjectDialog *dialog;
};

#endif // WIDGET_H
