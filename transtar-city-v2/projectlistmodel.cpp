﻿#include "projectlistmodel.h"

projectListModel::projectListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

QVariant projectListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    // FIXME: Implement me!
    if(role==Qt::DisplayRole){
        if(orientation==Qt::Horizontal){
            return 1;
        }
    }
    return QVariant();
}

int projectListModel::rowCount(const QModelIndex &parent) const
{
    if (!parent.isValid())
        return 0;

    // FIXME: Implement me!
}

QVariant projectListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    return QVariant();
}
