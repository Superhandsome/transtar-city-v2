﻿#include "projectutil.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QFile>

ProjectUtil::ProjectUtil()
{

}

void ProjectUtil::createProject(Project *project, QFile *file){
    //QFile saveJson()
    QJsonObject json;
    json=project->write();
    QJsonDocument jsonDoc(json);
    file->write(jsonDoc.toJson());

}

void ProjectUtil::saveProject(Project *project, QFile *file){
    ProjectUtil::createProject(project,file);
}

void ProjectUtil::openProject(Project *project, QFile *file){
    QByteArray saveData = file->readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    project->read(loadDoc.object());
}
