﻿#include "project.h"
#include <QJsonArray>
#include "projectdbnavigate.h"

double version=0.5;

Project::Project()
{
    this->isFirstCreate=false;   //初始化设置此值为false
}

int Project::getId() const
{
    return id;
}

void Project::setId(int value)
{
    id = value;
}

QString Project::getProjectName() const
{
    return projectName;
}

void Project::setProjectName(const QString &value)
{
    projectName = value;
}

QString Project::getProjectPath() const
{
    return projectPath;
}

void Project::setProjectPath(const QString &value)
{
    projectPath = value;
}

long Project::getLastUsedTime() const
{
    return createdTime;
}

void Project::setLastUsedTime(long value)
{
    createdTime = value;
}

QJsonObject Project::write(){
    QJsonObject json;
    json["version"]=version;
    json["project_name"]=projectName;
    QJsonArray array;
    for(int i=0;i<schemeList.size();++i){
        Scheme scheme=schemeList.at(i);
        QJsonObject scheme_json=scheme.write();
        array.append(scheme_json);
    }
    json["scenarios"]=array;
    return json;
}

void Project::read(QJsonObject json){
    this->projectName=json["project_name"].toString();
    QJsonArray schemeList=json["scenarios"].toArray();
    for(int i=0;i<schemeList.size();++i){
        Scheme scheme;
        scheme.read(schemeList.at(i).toObject());
        this->addScheme(scheme);
    }
    projectDbNavigate::updataLastUsedTime(this);
}

QVector<Scheme> &Project::getSchemeList()
{
    return schemeList;
}

bool Project::getIsFirstCreate() const
{
    return isFirstCreate;
}

void Project::setIsFirstCreate(bool value)
{
    isFirstCreate = value;
}

void Project::addScheme(Scheme scheme){
    schemeList.append(scheme);
}
