﻿#include "filestatewatcher.h"
#include <QDir>
#include <QDebug>

FileStateWatcher *FileStateWatcher::watcher=NULL;
FileStateWatcher::FileStateWatcher()
{
}

Scheme *FileStateWatcher::getScheme()
{
    return scheme;
}

Project *FileStateWatcher::getProject()
{
    return project;
}

FileStateWatcher *FileStateWatcher::getInstance(){    //单列模式
    if(watcher==NULL){
        watcher=new FileStateWatcher;
    }
    return watcher;
}

void FileStateWatcher::setActivedScheme(Scheme *scheme){
    this->scheme=scheme;
    fileEditMap=QMap<QString,FileEditState>();
}

void FileStateWatcher::setOpendProject(Project *project){
    this->project=project;
}

FileState FileStateWatcher::getFileState(QString fileName){
    Project *project=FileStateWatcher::getInstance()->getProject();
    Scheme *scheme=FileStateWatcher::getInstance()->getScheme();
    QString path=project->getProjectPath()+"/"+project->getProjectName()+scheme->getSchemeRelativePath();
    qDebug()<<"path是"<<path;
    QDir dir(path);
    dir.cd("data");
    if(!dir.exists()){
        dir.cdUp();
        dir.mkdir("data"); //如果data目录丢失，创建data目录
        return FileState::Losed;        //目录丢失，文件肯定丢失，直接返回lose
    }
    QString fileFullName=path+"/"+"data"+"/"+fileName;
    qDebug()<<fileFullName;
    QFile file(fileFullName);
    if(!file.exists()){
        return FileState::Losed;
    }else{
        return FileState::Normal;                  //目前无法判断文件是否损坏，所以如果文件未丢失就直接返回正常状态
    }
}

FileEditState FileStateWatcher::getFileEditState(QString fileFullName){
    if(fileEditMap.find(fileFullName)==fileEditMap.end()){
        return FileEditState::Saved;
    }else{
        return fileEditMap.value(fileFullName);                     //返回文件是否被修改
    }
}

void FileStateWatcher::setFileEditState(QString fileFullName, bool edited){
    if(edited)
        fileEditMap[fileFullName]=FileEditState::UnSaved;                 //改变文件是否被编辑的状态
    else
        fileEditMap[fileFullName]=FileEditState::Saved;
}
