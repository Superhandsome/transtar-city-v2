﻿#ifndef PROJECTDATABASE_H
#define PROJECTDATABASE_H

#include <QSqlDatabase>

class ProjectDbUtil
{
public:
    ProjectDbUtil();
    static bool createConnection();
};

#endif // PROJECTDATABASE_H
