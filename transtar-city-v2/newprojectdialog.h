﻿#ifndef NEWPROJECTDIALOG_H
#define NEWPROJECTDIALOG_H

#include <QDialog>
#include "project.h"

namespace Ui {
class NewProjectDialog;
}

class NewProjectDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewProjectDialog(QWidget *parent = 0);
    ~NewProjectDialog();
    Project *getProject() const;

public slots:
    void newProject();  //下一步按钮槽
    void cancle();   //取消按钮槽

    void getProjectNameEditContent(QString);
    void getProjectPathEditContent(QString);
    void getProjectPath();
signals:
    void nextClicked(Project project);
private:
    Ui::NewProjectDialog *ui;
    QString projectNameEditContent;
    QString projectPathEditContent;
    Project *project;
    void setNextButtonIsEnable();
};

#endif // NEWPROJECTDIALOG_H
