﻿#ifndef PROJECTUTIL_H
#define PROJECTUTIL_H

#include "project.h"
#include <QFile>
#include "scheme.h"

class ProjectUtil
{
public:
    ProjectUtil();
    static void saveProject(Project *project, QFile *file);       //保存项目变化到文件中
    static void createProject(Project *,QFile *);   //创建项目文件和文件夹
    static void createScheme(Scheme *,QFile *);   //创建方案文件夹
    static void openProject(Project *project, QFile *file);   //打开项目
};

#endif // PROJECTUTIL_H
