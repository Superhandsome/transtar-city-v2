﻿#include "newschemedialog.h"
#include "ui_newschemedialog.h"
#include <QHBoxLayout>
#include "scheme.h"
#include <QDir>
#include <QMessageBox>
#include <projectutil.h>
#include <QDateTime>
#include <QDebug>
#include "projectdbnavigate.h"
#include <QByteArray>
#include "transtarmainwindow.h"
#include "transtarwindowfactor.h"

NewSchemeDialog::NewSchemeDialog(Project *project, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewSchemeDialog)
{
    ui->setupUi(this);
    this->project=project;
    initBasicSchemeBox();   //初始基本方案combobox中的内容
    //this->setLayout(ui->horizontalLayout);
  //  this->resize(388,188);
    ui->okButton->setEnabled(false);
    ui->isActiveBox->setChecked(true);  //默认选中
    connect(ui->schemeNameEdit,SIGNAL(textChanged(QString)),this,SLOT(setOkButtonIsEnable(QString)));
    connect(ui->okButton,SIGNAL(clicked(bool)),this,SLOT(okButtonClicked()));
    connect(ui->cancleButton,SIGNAL(clicked(bool)),this,SLOT(cancelButtonClicked()));
}

NewSchemeDialog::~NewSchemeDialog()
{
    delete ui;
}

QString NewSchemeDialog::randomId(){

    const int LEN_ID=20;
    qsrand(QDateTime::currentDateTime().toTime_t());

    QString id;

    int i;
    for(i=0;i<LEN_ID;++i){
        char s='A'+qrand()%26;
        id.append(s);
    }

    return id;


}

void NewSchemeDialog::setOkButtonIsEnable(QString s){
    if(!s.isEmpty()){
        ui->okButton->setEnabled(true);
    }else{
        ui->okButton->setEnabled(false);
    }
}

void NewSchemeDialog::okButtonClicked(){  //定义确定按钮槽
    Scheme scheme;
    if(project->getSchemeList().size()==0){   //如果此项目下当前没有方案，则设为基本方案
        scheme.setIsBasic(true);
    }else{
        scheme.setIsBasic(false);
    }
    scheme.setBasedOn(ui->basicSchemeBox->currentData().toString());
    scheme.setIsActive(ui->isActiveBox->isChecked());   //设置是否是活动方案
    scheme.setSchemeName(ui->schemeNameEdit->text());
    scheme.setSchemeRelativePath("/"+scheme.getSchemeName());
    //char time[]=QDateTime::currentDateTime().toTime_t();

    scheme.setSchemeId(QString::number(QDateTime::currentDateTime().toTime_t())+this->randomId());     //设置方案id
    QString projectPath=project->getProjectPath();
    QDir dir(projectPath);                       //项目父目录
    if(project->getIsFirstCreate()){

        if(dir.exists(project->getProjectName())){
            QMessageBox::warning(this,"错误！","此项目在此路径下已存在！");
            return ;
        }else{
            dir.mkdir(project->getProjectName());   //创建项目目录
            if(!dir.cd(project->getProjectName())){
                qDebug()<<"没有进入项目目录";
                return ;
            }else{
                qDebug()<<"进入项目目录";

                QFile file(dir.filePath(project->getProjectName()+QString(".transpro")));
                if(!file.open(QIODevice::WriteOnly|QIODevice::Text)){
                    qDebug()<<"创建项目文件失败";
                    return;
                }
                dir.mkdir(scheme.getSchemeName());   //建立方案文件夹
                scheme.setSchemeRelativePath("/"+dir.relativeFilePath(scheme.getSchemeName()));
                project->addScheme(scheme);
                dir.cd(scheme.getSchemeName());
                dir.mkdir("data");                    //创建data目录
                ProjectUtil::createProject(project,&file);   //将项目信息写入json文件中保存

                projectDbNavigate::insertProject(project);  //项目创建完成之后在数据库中插入项目基本信息
            }
        }
    }else{
        dir.cd(project->getProjectName());    //进入项目文件夹
        QFile file(dir.filePath(project->getProjectName()+QString(".transpro")));    //打开项目文件
        if(!file.open(QIODevice::WriteOnly|QIODevice::Text)){
            qDebug()<<"打开项目文件失败";
            return;
        }
        if(ui->isActiveBox->isChecked()){
            qDebug()<<"新建的方案设为了活动方案";
            QVector<Scheme> &schemeList=project->getSchemeList();
            for(int i=0;i<schemeList.size();++i){
                if(schemeList.at(i).getIsActive()){

                    schemeList[i].setIsActive(false);
                    qDebug()<<schemeList.at(i).getSchemeName()<<schemeList[i].getIsActive();

                }
            }
        }
        dir.mkdir(scheme.getSchemeName());   //建立方案文件夹

        scheme.setSchemeRelativePath("/"+dir.relativeFilePath(scheme.getSchemeName()));
        dir.cd(scheme.getSchemeName());
        dir.mkdir("data");            //创建data目录
        project->addScheme(scheme);
        ProjectUtil::saveProject(project,&file);

    }
    int index=ui->basicSchemeBox->currentIndex();
    if(index!=0){          //0项为空，表示不基于其他方案，无需拷贝文件
        Scheme scheme_bebasiced=project->getSchemeList().at(index-1);     //根据当前项获得被基于的方案
        QString fromPath=project->getProjectPath()+"/"+project->getProjectName()+scheme_bebasiced.getSchemeRelativePath();  //获得被基于的方案的目录
        qDebug()<<fromPath;
        QString toPath=dir.currentPath()+"/"+project->getProjectName()+scheme.getSchemeRelativePath();
        qDebug()<<toPath;
        if(this->copyDirectoryFiles(fromPath,toPath)){
            qDebug()<<"succeed copy";
        }else{
            qDebug()<<"failed copy";
        }
    }
    project->setIsFirstCreate(false);   //设置false
    this->close();
    //this->deleteLater();
    TranstarMainWindow *transtarMainwindow=TranstarWindowFactor::getTranstarWindowInstance();  //单列模式 获得唯一主界面指针
    transtarMainwindow->setProject(*project);
    if(transtarMainwindow->isHidden()){
        transtarMainwindow->show();
    }
    emit createSucceed();
}

void NewSchemeDialog::initBasicSchemeBox(){
    QVector<Scheme> schemeList=project->getSchemeList();
    ui->basicSchemeBox->addItem(QString(""));//第一项为空，表示不基于其他方案
    qDebug()<<"已经有"<<schemeList.size()<<"方案";
    for(int i=0;i<schemeList.size();++i){
        ui->basicSchemeBox->addItem(schemeList.at(i).getSchemeName());
        if(schemeList[i].getIsBasic()){
            ui->basicSchemeBox->setCurrentIndex(i+1);    //默认设为基础方案
        }
    }

}

void NewSchemeDialog::cancelButtonClicked(){
    this->close();
    //this->deleteLater();
}

bool NewSchemeDialog::copyDirectoryFiles(QString fromDir,QString toDir)
{
    QDir sourceDir(fromDir);
    QDir targetDir(toDir);
    if(!targetDir.exists()){    /**< 如果目标目录不存在，则进行创建 */
        if(!targetDir.mkdir(targetDir.absolutePath()))
            return false;
    }

    QFileInfoList fileInfoList = sourceDir.entryInfoList();
    foreach(QFileInfo fileInfo, fileInfoList){
        if(fileInfo.fileName() == "." || fileInfo.fileName() == "..")
            continue;

        if(fileInfo.isDir()){    /**< 当为目录时，递归的进行copy */
            if(!copyDirectoryFiles(fileInfo.filePath(),
                targetDir.filePath(fileInfo.fileName())))
                return false;
        }
        else{
            if(targetDir.exists(fileInfo.fileName())){
                targetDir.remove(fileInfo.fileName());
            }

            /// 进行文件copy
            if(!QFile::copy(fileInfo.filePath(),
                targetDir.filePath(fileInfo.fileName()))){
                    return false;
            }
        }
    }
    return true;
}
