﻿#include "newprojectdialog.h"
#include "ui_newprojectdialog.h"
#include <QGridLayout>
#include <project.h>
#include "newschemedialog.h"
#include <QDateTime>
#include <QDir>
#include <QFileDialog>

NewProjectDialog::NewProjectDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewProjectDialog)
{
    ui->setupUi(this);
    //this->setLayout(ui->gridLayout);
    //ui->buttonBox->button();
    ui->nextpushButton->setEnabled(false);   //没有输入内容时，下一步按钮不可以

    connect(ui->projectNameEdit,SIGNAL(textChanged(QString)),this,SLOT(getProjectNameEditContent(QString)));

    connect(ui->projectPathEdit,SIGNAL(textChanged(QString)),this,SLOT(getProjectPathEditContent(QString)));

    connect(ui->canclepushButton,SIGNAL(clicked(bool)),this,SLOT(cancle()));

    connect(ui->nextpushButton,SIGNAL(clicked(bool)),this,SLOT(newProject()));

    connect(ui->findButton,SIGNAL(clicked(bool)),this,SLOT(getProjectPath()));

   // this->resize(488,188);
}

NewProjectDialog::~NewProjectDialog()
{
    delete ui;
}

void NewProjectDialog::cancle(){
    this->close();
   // this->deleteLater();
}

void NewProjectDialog::newProject(){   //定义下一步按钮槽
    project=new Project;
    project->setProjectName(ui->projectNameEdit->text());
    project->setProjectPath(QDir::fromNativeSeparators(ui->projectPathEdit->text()));   //转变分割符方式
    project->setIsFirstCreate(true);  //设置第一次创建项目，第一次创建项目的时候需要同时创建基本方案
    project->setLastUsedTime(QDateTime::currentDateTime().toTime_t());
    emit nextClicked(*project);


}

void NewProjectDialog::setNextButtonIsEnable(){
    if(!projectNameEditContent.isEmpty()&&!projectPathEditContent.isEmpty()){  //当项目名和项目路径为空的时候设置下一步按钮不可用
        ui->nextpushButton->setEnabled(true);
    }else{
        ui->nextpushButton->setEnabled(false);
    }
}

void NewProjectDialog::getProjectNameEditContent(QString s){
    projectNameEditContent=s;
    setNextButtonIsEnable();
}

void NewProjectDialog::getProjectPathEditContent(QString s){
    projectPathEditContent=s;
    setNextButtonIsEnable();
}

void NewProjectDialog::getProjectPath(){
    QString directory=QFileDialog::getExistingDirectory(this,"选择文件夹",QCoreApplication::applicationDirPath());
    ui->projectPathEdit->setText(directory);
}

Project *NewProjectDialog::getProject() const
{
    return project;
}
