﻿#include "projectlistframe.h"
#include <QVBoxLayout>
#include <QPainter>
#include <QBrush>
#include <QDebug>
#include <QDir>
#include "projectutil.h"
#include <QMetaType>

projectListFrame::projectListFrame(QWidget *parent):QFrame(parent)
{
    qRegisterMetaType<Project>("Project");
    QVBoxLayout *layout=new QVBoxLayout(this);
    projectName=new QLabel(this);
    projectPath=new QLabel(this);
    layout->addWidget(projectName);
    layout->addWidget(projectPath);
    layout->setSpacing(0);
    layout->setMargin(0);
   // this->setFrameShape(QFrame::Box);
    this->setLayout(layout);
    is_enter=false;
}


void projectListFrame::mousePressEvent(QMouseEvent *event){
    qDebug()<<"鼠标点击了";
    if(event->button()==Qt::LeftButton){
       emit openProject(project);
    }
}

Project projectListFrame::getProject() const
{
    return project;
}

void projectListFrame::setProject(const Project &value)
{
    project = value;
}


void projectListFrame::setProjectShowOnLabel(){
    projectName->setText(project.getProjectName());
    //ui->projectNameLabel->setTextFormat();
    projectPath->setText(project.getProjectPath());
}

void projectListFrame::enterEvent(QEvent *event){
    is_enter=true;
    repaint();
}

void projectListFrame::paintEvent(QPaintEvent *event){

    if(is_enter){
        QPainter painter(this);
        painter.setRenderHint(QPainter::Antialiasing,true);
        //painter.setBrush();
        QRect rect=frameRect();
        QBrush blue(Qt::blue);
        QPen pen(Qt::white);
        painter.setPen(pen);
        painter.setBrush(blue);
        painter.drawRect(rect);
        //qDebug()<<"重新绘制"<<this->geometry();
        projectName->setStyleSheet("color:white");
        projectPath->setStyleSheet("color:white");
    }else{
        QFrame::paintEvent(event);
        projectName->setStyleSheet("color:black");
        projectPath->setStyleSheet("color:black");
    }


}

void projectListFrame::leaveEvent(QEvent *event){
    //qDebug()<<"离开";
    is_enter=false;
    repaint();
}

