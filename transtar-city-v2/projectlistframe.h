﻿#ifndef PROJECTLISTFRAME_H
#define PROJECTLISTFRAME_H

#include <QWidget>
#include <QFrame>
#include "project.h"
#include <QMouseEvent>
#include <QLabel>
#include <QEvent>
#include <QPaintEvent>

class projectListFrame : public QFrame
{
    Q_OBJECT
public:
    projectListFrame(QWidget *parent=0);
    void mousePressEvent(QMouseEvent *event);
    void enterEvent(QEvent *event);
    Project getProject() const;
    void setProject(const Project &value);
    void setProjectShowOnLabel();
    void leaveEvent(QEvent *event);
    void paintEvent(QPaintEvent *event);
signals:
    void openProject(Project project);
private:
    bool is_enter;
    Project project;
    QLabel *projectName;
    QLabel *projectPath;
};

#endif // MYFRAME_H
