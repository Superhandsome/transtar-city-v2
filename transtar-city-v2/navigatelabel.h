﻿#ifndef NAVIGATELABEL_H
#define NAVIGATELABEL_H
#include <QLabel>

enum NavigateType{ //标签类型，是新建项目还是打开项目
    CREATE=1,
    OPEN=2
};

class NavigateLabel:public QLabel
{
    Q_OBJECT
public:
    NavigateLabel(QString text,QWidget *parent=0);
    void mousePressEvent(QMouseEvent *event);  //鼠标单击事件
    NavigateType getType() const;
    void setType(const NavigateType &value);
signals:
    void createSignal();
    void openSignal();
private:
    NavigateType type;   //标签类型

};

#endif // NAVIGATELABEL_H
