﻿#include "helpwidget.h"
#include "ui_helpwidget.h"
#include <QHelpEngine>
#include "helpbrowser.h"
#include <QSplitter>
#include <QTabWidget>
#include <QVBoxLayout>

HelpWidget::HelpWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HelpWidget)
{
    ui->setupUi(this);
    initHelpWidget();
}

HelpWidget::~HelpWidget()
{
    delete ui;
}

void HelpWidget::initHelpWidget(){
    QHelpEngine* helpEngine = new QHelpEngine(
                QApplication::applicationDirPath() +
                "/helpdocuments/transtarhelp.qhc");        //当前目录下的helpdocuments存放帮助系统所用文件
    helpEngine->setupData();

    QTabWidget* tWidget = new QTabWidget;
    tWidget->setMaximumWidth(200);
    tWidget->addTab(helpEngine->contentWidget(), "内容");
    tWidget->addTab(helpEngine->indexWidget(), "索引");

    HelpBrowser *textViewer = new HelpBrowser(helpEngine);
    textViewer->setSource(
                QUrl("qthelp://help.transtar/doc/HOME.html"));
    connect(helpEngine->contentWidget(),
            SIGNAL(linkActivated(QUrl)),
            textViewer, SLOT(setSource(QUrl)));

    connect(helpEngine->indexWidget(),
            SIGNAL(linkActivated(QUrl, QString)),
            textViewer, SLOT(setSource(QUrl)));

    QSplitter *horizSplitter = new QSplitter(Qt::Horizontal);
    horizSplitter->insertWidget(0, tWidget);
    horizSplitter->insertWidget(1, textViewer);
   // horizSplitter->hide();

//    helpWindow = new QDockWidget(tr("Help"), this);
//    helpWindow->setWidget(horizSplitter);
//    helpWindow->hide();
//    addDockWidget(Qt::BottomDockWidgetArea, helpWindow);
    QVBoxLayout *layout=new QVBoxLayout(this);
    layout->addWidget(horizSplitter);
}

