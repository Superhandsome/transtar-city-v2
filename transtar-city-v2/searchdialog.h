﻿#ifndef SEARCHDIALOG_H
#define SEARCHDIALOG_H

#include <QDialog>
#include <QTableView>
#include <utilmodel.h>
#include <QVector>
#include <QMap>
#include <QMapIterator>

namespace Ui {
class SearchDialog;
}

class SearchDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SearchDialog(QWidget *parent = 0,QTableView *view=0,UtilModel *model=0);
    ~SearchDialog();
    QString getInputText();

public slots:
    void search();
private:
    Ui::SearchDialog *ui;
    QTableView *view;
    UtilModel *model;
    QMap<int,int>::iterator searchItem;     //保存当前查找所在位置

    QMap<int,int> *result;
    QString searchContent;
    void searchNext();
};

#endif // SEARCHDIALOG_H
