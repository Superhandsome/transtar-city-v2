﻿#include "utilmodel.h"
#include <QDebug>
#include <QMessageBox>
#include <QRegExp>
#include "filestatewatcher.h"

UtilModel::UtilModel(QObject *parent)
    : QAbstractTableModel(parent)
{
    count=1;
    progressDialog=NULL;
    connect(this,SIGNAL(readDataNowAt()),this,SLOT(changeProgressDialogNum()));
    selectedItemRow=-1;
    selectedItemColumn=-1;
    modifyTag=false;
}

QVariant UtilModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  //  qDebug()<<"invoke model header";
    // FIXME: Implement me!
    if(role==Qt::DisplayRole){
        if(orientation==Qt::Vertical){
            return section;          //竖直方向无表头，仅显示行号
        }else{
            if(!horizontalHeader->isEmpty()){
                //qDebug()<<horizontalHeader->at(section);
                return horizontalHeader->at(section);   //若有水平方向的表头，则显示表头，
            }
            else{
                return section;                //无水平方向表头，显示列号
            }
        }
    }
    return QVariant();
}

int UtilModel::rowCount(const QModelIndex &parent) const
{
   // qDebug()<<"invoke model row";
//    if (!parent.isValid())
//        return 0;
    //qDebug()<<fileData->size();
    return fileData->size();
    // FIXME: Implement me!
}

int UtilModel::columnCount(const QModelIndex &parent) const
{
   // qDebug()<<"invoke model column";
//    if (!parent.isValid())
//        return 0;
  //  qDebug()<<fileData->at(0).size();
    if(parent.isValid()){
        return 0;
    }

     return fileData->at(0).size();  //每一行的列数,每一行列数都相同，直接返回第一行列数

    // FIXME: Implement me!
}

QVariant UtilModel::data(const QModelIndex &index, int role) const
{
   // qDebug()<<"invoke model data";
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    if(role==Qt::TextAlignmentRole){
        if(index.column()!=fileData->at(0).size()-1)   //不是最后一行，居右显示
            return int(Qt::AlignRight|Qt::AlignVCenter);
        else{
            return int(Qt::AlignCenter|Qt::AlignVCenter);
        }
    }else if(role==Qt::DisplayRole){

        int row=index.row();
        int column=index.column();
        if(count<=fileData->size())
            emit readDataNowAt();

        QVector<QVariant> dataList;
        if(row<fileData->size()){
            dataList=fileData->at(row);
            if(column<dataList.size()){
              //  qDebug()<<dataList.at(column);
                return dataList.at(column);
            }else{
                return QVariant("");
            }
        }else{
            return QVariant("");
        }

       // qDebug()<<dataList.at(column);

    }/*else if(role==Qt::BackgroundRole){
        if(index.row()==selectedItemRow&&index.column()==selectedItemColumn)
            return QVariant(QColor(Qt::blue));
    }*/


    return QVariant();
}

void UtilModel::setHorizontalHeader(QVector<QString> *value)
{
    horizontalHeader = value;
    qDebug("succeed setHorizontalHeader");
}

void UtilModel::setFileData(QVector<QVector<QVariant> > *value)
{
    fileData = value;
    qDebug()<<fileData->size();
}

void UtilModel::setFileUtil(FileUtil *value)
{
    fileUtil = value;
    qDebug("succeed setFileUtil");
}

void UtilModel::printData(){
    for(int i=0;i<fileData->size();++i){
        QVector<QVariant> dataList=fileData->at(i);
        int length=dataList.size();
        for(int j=0;j<length;++j)
            qDebug()<<dataList.at(j);
       // qDebug()<<'\n';
    }
}

void UtilModel::printHeader(){
    qDebug()<<"输出表头信息";
    for(int i=0;i<this->horizontalHeader->size();++i){
        qDebug()<<horizontalHeader->at(i);
    }
}

bool UtilModel::setData(const QModelIndex &index, const QVariant &value, int role){
    int row=index.row();
    int column=index.column();

    if(index.isValid()&&role==Qt::EditRole){
        QString oddData=fileData->at(row).at(column).toString();
        if(value.toString()==oddData){   //数据没有变化不用改变
            return false;
        }
        qDebug()<<fileUtil->getColumntype(column);
       // QMessageBox::information(NULL,"Type Infomation",QString(value.type())+' '+QString(value.typeName()));
        if(fileUtil->getColumntype(column)=="QString"){

            if(!value.toString().isEmpty()){
                fileUtil->setContentsData(row,column,value);
                modifyTag=true;
                emit dataChanged(index,index);
                FileStateWatcher::getInstance()->setFileEditState(fileUtil->getFileFullName());  //状态机保存文件编辑却未保存状态
                return true;
            }else{
                QMessageBox::information(NULL,"Error",QString("This item can't be empty!"));
                return false;
            }
            if(value.toString().isEmpty()){
                QMessageBox::warning(NULL,"错误！","内容不能为空！");
                return false;
            }

            fileUtil->setContentsData(row,column,value);
            modifyTag=true;
            emit dataChanged(index,index);
            FileStateWatcher::getInstance()->setFileEditState(fileUtil->getFileFullName());  //状态机保存文件编辑却未保存状态
            return true;

        }else if(fileUtil->getColumntype(column)=="int"){

            QRegExp regExp("[0-9]{1,20}");     //匹配整形值
            if(regExp.exactMatch(value.toString())){
                qDebug("match");
                fileUtil->setContentsData(row,column,value);
                modifyTag=true;
                emit dataChanged(index,index);
                FileStateWatcher::getInstance()->setFileEditState(fileUtil->getFileFullName());  //状态机保存文件编辑却未保存状态
                return true;
            }else{
                qDebug("didn't match");
                QMessageBox::information(NULL,"类型错误！","此列数据要求是整型！");
                return false;
            }

        }else if(fileUtil->getColumntype(column)=="double"){
            QRegExp regExpDouble("*.*");  //小数
            regExpDouble.setPatternSyntax(QRegExp::Wildcard);
            QRegExp regExp("[0-9]{1,20}");  //整数
            if(regExp.exactMatch(value.toString())||regExpDouble.exactMatch(value.toString())){
                qDebug("match");
                fileUtil->setContentsData(row,column,value);
                modifyTag=true;
                emit dataChanged(index,index);
                FileStateWatcher::getInstance()->setFileEditState(fileUtil->getFileFullName());  //状态机保存文件编辑却未保存状态
                return true;
            }else{
                qDebug("didn't match");
                QMessageBox::information(NULL,"类型错误！","此列数据要求是浮点型！");
                return false;
            }
        }else{
            QMessageBox::information(NULL,"类型错误","没有发现匹配的数据类型！");
            return false;
        }
    }
    return false;
}

Qt::ItemFlags UtilModel::flags(const QModelIndex &index) const{
    Qt::ItemFlags flags=QAbstractItemModel::flags(index);
    int column=index.column();
    if(index.isValid()&&this->fileUtil->isEditable(column)){
        flags|=Qt::ItemIsEditable;
    }
    return flags;
}

void UtilModel::changeProgressDialogNum(){
//    if(progressDialog==NULL){          //第一次调用此函数的时候加载

//        progressDialog=new QProgressDialog();
//        progressDialog->setMinimum(0);
//        progressDialog->setMaximum(fileData->size());
//    }

//    progressDialog->setValue(count);
//    progressDialog->setLabelText(QString("Read data at row %1 ,there all %2 rows").arg(count).arg(fileData->size()));
//    ++count;
}

QMap<int, int>* UtilModel::search(QString s){
    result=new QMap<int,int>;
    for(int i=0;i<fileData->size();++i){
        QVector<QVariant> data=fileData->at(i);
        for(int j=0;j<data.size();++j){
            if(s==data.at(j).toString()){
                result->insert(i,j);   //往结果集中插入行、列

            }
        }
    }
    return result;
}

bool UtilModel::isModifyed(){
    return modifyTag;
}

void UtilModel::setModifyTag(bool value)
{
    modifyTag = value;
}
