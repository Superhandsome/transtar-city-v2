﻿#include "searchdialog.h"
#include "ui_searchdialog.h"

SearchDialog::SearchDialog(QWidget *parent, QTableView *view, UtilModel *model) :
    QDialog(parent),
    ui(new Ui::SearchDialog)
{
    ui->setupUi(this);
    this->view=view;
    this->model=model;

    result=NULL;

    connect(ui->searchButton,SIGNAL(clicked(bool)),this,SLOT(search()));
}

SearchDialog::~SearchDialog()
{
    model->selectedItemColumn=-1;
    model->selectedItemRow=-1;
    delete ui;
}

QString inline SearchDialog::getInputText(){
    return ui->lineEdit->text();
}

void SearchDialog::search(){
//    if(searchContent.isEmpty()){
//        searchContent=ui->lineEdit->text();
//    }
    if(result==NULL||searchContent!=ui->lineEdit->text()){
        searchContent=ui->lineEdit->text();
        result=new QMap<int ,int>;
        result=model->search(ui->lineEdit->text());
        searchItem=result->begin();
    }
    searchNext();
}

void SearchDialog::searchNext(){

    if(searchItem==result->end()){
        searchItem=result->begin();        //重新置位0，从头查找
    }

    int row=searchItem.key();
    int column=searchItem.value();

    //QWidget *item=view->childAt(row,column);
    //item->setFocus();
    model->selectedItemRow=row;
    model->selectedItemColumn=column;
    view->selectionModel()->select(model->index(row,column),QItemSelectionModel::SelectCurrent);

    view->setCurrentIndex(model->index(row,column));  // 若匹配的单元格在窗口外，将匹配的单元格移动到窗口上显示
  //  model->data(model->index(row,column),Qt::BackgroundRole);
    this->clearFocus();
    view->activateWindow();     //让view获得焦点
    view->setEnabled(true);
    view->setFocus(Qt::MouseFocusReason);

//    view->clearFocus();
//    this->activateWindow();
//    this->setFocus();
    searchItem++; //后移
}
