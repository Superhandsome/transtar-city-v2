﻿#include "widget.h"
#include "ui_widget.h"
#include <QListWidgetItem>
#include <QStringListModel>
#include <QVBoxLayout>
#include <QDebug>
#include "navigatelabel.h"
#include <QModelIndex>
#include "projectinfowidget.h"
#include "projectdbnavigate.h"
#include <projectlistframe.h>
#include "newprojectdialog.h"
#include <QFileDialog>
#include "projectutil.h"
#include "transtarmainwindow.h"
#include <QMetaType>
#include "transtarwindowfactor.h"
#include "newschemedialog.h"
#include "projectdbnavigate.h"
#include <QDateTime>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    //qRegisterMetaType<Project> ("Project");
    //ui->navigatelistView->setSpacing(20);
    ui->navigateframe->setLayout(ui->navigateLayout);
    ui->showrecentframe->setStyleSheet("{background-color:white;}");
    //setStyleSheet("projectInfoWidget:hover{background-color:blue;}");
    dialog=NULL;
    initNavigateListWidget();
    initProjectListWidget();

}

Widget::~Widget()
{
    delete ui;
}

void Widget::initNavigateListWidget(){
    NavigateLabel *label=new NavigateLabel("新建项目");
    connect(label,SIGNAL(createSignal()),this,SLOT(createProjectSlot())); //链接信号和槽

    label->setType(NavigateType::CREATE);   //设置创建项目
    label->setFrameStyle(QFrame::Plain);
    label->setStyleSheet(":hover{background-color:#bcd8ef;}");
    label->setMinimumHeight(50);
    label->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Minimum);
    NavigateLabel *label2=new NavigateLabel("打开项目");
    connect(label2,SIGNAL(openSignal()),this,SLOT(openProjectSlot())); //链接信号和槽

    label2->setType(NavigateType::OPEN);
    label2->setFrameStyle(QFrame::Plain);
    label2->setStyleSheet(":hover{background-color:#bcd8ef;}");
    label2->setMinimumHeight(50);
    label2->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Minimum);
    if(ui->navigatelistView->layout()!=NULL){
        qDebug()<<"delete";
        delete ui->navigatelistView->layout();
    }
    QVBoxLayout *layout=new QVBoxLayout;


    layout->addWidget(label);
    layout->addWidget(label2);
    layout->addStretch();
    ui->navigatelistView->setLayout(layout);
   // ui->navigatelistView->setIndexWidget(QModelIndex(),label2);

}
void Widget::initProjectListWidget(){
    //this->setStyleSheet("projectListFrame:hover{background-color:#bcd8ef}");
    QVector<Project> projectList=projectDbNavigate::selectProject();
   // QVBoxLayout *layout=new QVBoxLayout;
    for(int i=0;i<projectList.count();++i){
        projectListFrame *projectWidget=new projectListFrame(this);
        connect(projectWidget,SIGNAL(openProject(Project)),this,SLOT(openProjectSlot_frame(Project)));  //链接信号和槽

        projectWidget->setMinimumHeight(52);
        projectWidget->setMinimumWidth(ui->showrecentframe->width());
        projectWidget->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Minimum);
        projectWidget->setProject(projectList.at(i));
        projectWidget->setProjectShowOnLabel();
        ui->projectListLayout->addWidget(projectWidget);
       // projectWidget->setStyleSheet("::hover{background-color:blue;}");

    }
    ui->projectListLayout->addStretch();
    //layout->setSizeConstraint(QLayout::SetFixedSize);
    //layout->setSpacing(0);
    //layout->setMargin(0);
    //ui->projectlistView->setLayout(layout);
    ui->showrecentframe->setLayout(ui->projectListLayout);

}

void Widget::createProjectSlot(){
    qDebug()<<"创建项目";  //测试这个事件处理函数被调用
//     QFileDialog::getSaveFileName(this->parent(),);
    dialog=new NewProjectDialog(this);
    connect(dialog,SIGNAL(nextClicked(Project)),this,SLOT(createNewSchemeDialog(Project)));
    dialog->exec();
//    this->close();
//    this->deleteLater();
}

void Widget::openProjectSlot(){
    qDebug()<<"打开项目";
    //弹出文件选择框，打开项目
    QString fileName=QFileDialog::getOpenFileName(this,"选择项目",QCoreApplication::applicationDirPath(),"Files(*.transpro)");
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly|QIODevice::Text)){
        qDebug()<<"打开项目文件失败";
        return ;
    }
    Project project;

    ProjectUtil::openProject(&project,&file);
    QDir dir(fileName);
    dir.cdUp();
    //qDebug()<<"当前目录是"<<dir.currentPath();
    project.setProjectPath(dir.currentPath());
    project.setLastUsedTime(QDateTime::currentDateTime().toTime_t());
    QVector<Project> projectList=projectDbNavigate::selectProject();
    bool find_tag=false;
    for(int i=0;i<projectList.size();++i){
        if(projectList.at(i).getProjectName()==project.getProjectName()&&projectList.at(i).getProjectPath()==project.getProjectPath()){
            find_tag=true;
            break;
        }
    }
    if(find_tag==false){    //此项目的记录不在数据库中时，插入数据库中
        qDebug()<<"此项目不在数据库中";
        qDebug()<<project.getProjectName()<<project.getProjectPath()<<project.getLastUsedTime();
        projectDbNavigate::insertProject(&project);
    }

    TranstarMainWindow *mainwindow=TranstarWindowFactor::getTranstarWindowInstance();
    mainwindow->setProject(project);
    mainwindow->show();
//            this->parentWidget()->close();
//            this->parentWidget()->deleteLater();
    qDebug()<<project.getProjectName();
    this->close();
    //this->deleteLater();
}

void Widget::openProjectSlot_frame(Project project){
    QDir dir(project.getProjectPath());
    dir.cd(project.getProjectName());

    QFile file(dir.filePath(project.getProjectName()+QString(".transpro")));
    if(!file.open(QIODevice::ReadOnly|QIODevice::Text)){
        qDebug()<<"创建项目文件失败";
        return;
    }

    ProjectUtil::openProject(&project,&file);
    qDebug()<<project.getSchemeList().size();
    TranstarMainWindow *mainwindow=TranstarWindowFactor::getTranstarWindowInstance();
    mainwindow->setProject(project);
    if(mainwindow->isHidden())
        mainwindow->show();
    this->close();
}

void Widget::createNewSchemeDialog(Project pro){
    Project project=pro;
    NewSchemeDialog *schemeDialog=new NewSchemeDialog(&project,this);
    connect(schemeDialog,SIGNAL(createSucceed()),this,SLOT(closeThis()));
    schemeDialog->exec();
}

void Widget::closeThis(){
    if(dialog!=NULL){
        dialog->close();
    }
    this->close();
    //this->deleteLater();
}
