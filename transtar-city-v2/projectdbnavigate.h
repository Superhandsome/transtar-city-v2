﻿#ifndef PROJECTDBNAVIGATE_H
#define PROJECTDBNAVIGATE_H

#include <QObject>
#include "project.h"
#include <QVector>

class projectDbNavigate
{
public:
    projectDbNavigate();
    static QVector<Project> selectProject();
    static void insertProject(Project *);
    static void updataLastUsedTime(Project *project);
};

#endif // PROJECTDBNAVIGATE_H
