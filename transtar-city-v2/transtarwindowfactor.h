﻿#ifndef TRANSTARWINDOWFACTOR_H
#define TRANSTARWINDOWFACTOR_H
#include "transtarmainwindow.h"

class TranstarWindowFactor
{
public:
    TranstarWindowFactor();
    static TranstarMainWindow *getTranstarWindowInstance();
private:
    static TranstarMainWindow *window;
};

#endif // TRANSTARWINDOWFACTOR_H
