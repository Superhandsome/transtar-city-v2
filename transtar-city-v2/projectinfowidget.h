﻿#ifndef PROJECTINFOWIDGET_H
#define PROJECTINFOWIDGET_H

#include <QWidget>
#include <QMouseEvent>
#include <project.h>
#include <QMouseEvent>
#include <QFrame>

namespace Ui {
class projectInfoWidget;
}

class projectInfoWidget : public QFrame
{
    Q_OBJECT

public:
    explicit projectInfoWidget(QWidget *parent = 0);
    ~projectInfoWidget();
    void mousePressEvent(QMouseEvent *event);
    Project getProject() const;
    void setProject(const Project &value);
    void setProjectShowOnLabel();

private:
    Ui::projectInfoWidget *ui;
    Project project;
};

#endif // PROJECTINFOWIDGET_H
