﻿#ifndef TRANSTARMAINWINDOW_H
#define TRANSTARMAINWINDOW_H

#include <QMainWindow>
#include "project.h"
#include <QDockWidget>
#include <QTabWidget>
#include <QMenu>
#include <QPoint>
#include <QTreeView>
#include <QStandardItemModel>
#include "newprojectdialog.h"
#include <QAction>
#include <QToolBar>
#include "datawindow.h"
#include <QCloseEvent>

namespace Ui {
class TranstarMainWindow;
}

class TranstarMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit TranstarMainWindow(QWidget *parent = 0);
    ~TranstarMainWindow();

    Project getProject() const;
    void setProject(Project value);
public slots:
    void closeTabWin(int index);
    void setContents(QPoint point);  //设置QTreeView上下文

    void newProject();    //新建项目槽
    void newScheme();       //新建方案槽
    void openProject() ;   //打开项目槽
    void openRecentProject(Project project);   //打开最近使用项目槽

    void closeProject(); //关闭项目槽
    void setSchemeisBasic();
    void setSchemeisActive() ;     //设置方案是基础或者活动 槽
    void deleteScheme();    //删除方案槽
    void createNewSchemeDialog(Project pro);
    void closeAndUpdate();
    void initChoseDataWidget();      //初始化数据文件选择窗口
    void showHelpWidget();    //打开帮助窗口

    void openDataFile(QString fileName);    //打开文件
    void closeEvent(QCloseEvent *event);
private:
    Ui::TranstarMainWindow *ui;
    NewProjectDialog *dialog;
    void initTabWidget();
    // 窗口框架管理
    void initWinFramework(); ///< 初始化窗口主体框架
    void addDockToLeft(QDockWidget* dock, bool show=true); ///< 添加到左侧dock区域，折叠的形式。show表示是否立即显示出来
    void addDockToLeftBottom(QDockWidget* dock, bool show=true); ///< 添加到左下角dock区域，折叠的形式
    void addDockToBottom(QDockWidget* dock, bool show=true); ///< 添加到下方dock区域，折叠的形式
    void addDockToRight(QDockWidget* dock, bool show=true); ///< 添加到右侧角dock区域，折叠的形式
    void addDockToRightBottom(QDockWidget* dock, bool show=true); ///< 添加到右下角dock区域，折叠的形式
    int addWinToCenter(QWidget* widget, QString tabName, bool show=true); ///< 将窗口添加到主窗口

    //void buildToolBar(); //建立工具栏

    void initProjectTreeView();
    void initRecentProject();
    void initContentMenu();

    bool DeleteDirectory(const QString &path) ;

    void saveFile();
   // void setCurrentActiveScheme();
    // 5个dock锚点
    QDockWidget* mLeftAnchorDock;
    QDockWidget* mLeftBottomAnchorDock;
    QDockWidget* mBottomAnchorDock;
    QDockWidget* mRightAnchorDock;
    QDockWidget* mRightBottomAnchorDock;

    QTabWidget* mTabWidget; ///< 窗口容器
    //QMainWindow *tabWindow;       //为了在tabwidget上显示工具栏，得把tabwidget套在一个mainwindow中

    QMenu *project_Menu;   //右键项目出现的上下文菜单
    QMenu *scheme_Menu;     //方案上下文菜单
    QTreeView *view;
    QStandardItemModel *model;
    Project project;   //主界面当前项目
    Scheme activeScheme;   //活动方案
    QPoint point;

    QStandardItem *active_item;
    QStandardItem *basic_item;

//    QAction *saveAction;     //保存文件
//    QAction *searchAction;      //查找
//    QAction *closeAction;   //关闭

//    QToolBar *commonToolBar;     //公共工具栏

    DataWindow *dataWindow;

    QFile *file;         //当前打开的文件指针

    int fileIndex;     //显示数据文件的窗口在tabWidget中的index
};

#endif // TRANSTARMAINWINDOW_H
