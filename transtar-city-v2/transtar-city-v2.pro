#-------------------------------------------------
#
# Project created by QtCreator 2017-03-17T15:19:03
#
#-------------------------------------------------

QT       += core gui sql help

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = transtar-city-v2
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    navigatelabel.cpp \
    projectdbUtil.cpp \
    projectdbnavigate.cpp \
    project.cpp \
    projectinfowidget.cpp \
    newprojectdialog.cpp \
    scheme.cpp \
    projectutil.cpp \
    newschemedialog.cpp \
    transtarmainwindow.cpp \
    projectlistframe.cpp \
    projectaction.cpp \
    transtarwindowfactor.cpp \
    chosedatawidget.cpp \
    helpwidget.cpp \
    helpbrowser.cpp \
    filestatewatcher.cpp \
    fileutil.cpp \
    lineeditdelegate.cpp \
    searchdialog.cpp \
    utilmodel.cpp \
    datawindow.cpp

HEADERS  += widget.h \
    navigatelabel.h \
    projectinfowidget.h \
    projectdbUtil.h \
    projectdbnavigate.h \
    project.h \
    newprojectdialog.h \
    scheme.h \
    projectutil.h \
    newschemedialog.h \
    transtarmainwindow.h \
    projectlistframe.h \
    projectaction.h \
    transtarwindowfactor.h \
    chosedatawidget.h \
    helpwidget.h \
    helpbrowser.h \
    filestatewatcher.h \
    fileutil.h \
    lineeditdelegate.h \
    searchdialog.h \
    utilmodel.h \
    datawindow.h

FORMS    += widget.ui \
    projectinfowidget.ui \
    newprojectdialog.ui \
    newschemedialog.ui \
    transtarmainwindow.ui \
    chosedatawidget.ui \
    helpwidget.ui \
    searchdialog.ui \
    datawindow.ui

RESOURCES += \
    img.qrc
