﻿#ifndef FILEUTIL_H
#define FILEUTIL_H

#include <QFile>
#include <QTextStream>
#include <QVector>

class FileUtil
{
public:
    FileUtil();
    void setFile(QFile *file);   //设置操作的文件
    void read(int skipHeaderLength);   //读文件通用函数，特殊文件用其他专用方法
    void readConn()  ;            //专门读coon.txt文件
    void readBusStopFile() ;     //专门读文件名以BusStop开头的文件
   // void readBusVehBusorBRT();    //读BusVehBus.txt  BusVehBRT.txt文件


    bool setFileConfine();
    QVector<QVector<QVariant>> getSkipContents();
    void setSkipContents(const QVector<QVector<QVariant> > &value);
    bool openFile(QFile *f=NULL);
    QVector<QString>* getColumn_name();

    QVector<QVector<QVariant>>* getContents();

    QString getColumntype(int ) const;

    QVector<bool> getColumn_changeable() const;
    bool isEditable(int column);   //   返回此列是否可编辑
    void setContentsData(int row,int column,QVariant data);

    void saveFile();
    QString getFileFullName() const;

private:
    QFile *file;
    QString fileName;
    QString fileFullName;   //包含路径
    QTextStream *stream;
    QVector<QVector<QVariant>> SkipedContents;
    QVector<QVector<QVariant>> Contents;
    QVector<QString> column_name;
    QVector<QString> column_type;
    QVector<bool> column_changeable;
    int skipLength;
};

#endif // FILEUTIL_H
