﻿#include "fileutil.h"
#include <QFileDialog>
#include <QDebug>
#include <QMessageBox>
#include <QStringList>

FileUtil::FileUtil()
{

}

void FileUtil::setFile(QFile *file){
    this->file=file;
    this->stream=new QTextStream(this->file);   //设置流

}

QVector<QVector<QVariant> > FileUtil::getSkipContents()
{
    return SkipedContents;
}

void FileUtil::setSkipContents(const QVector<QVector<QVariant>> &value)
{
    SkipedContents = value;
}

void FileUtil::read(int skipHeaderLength){
    qDebug()<<"read is been invoked";
    int skip=0;
    while(!stream->atEnd()){

        if(skip<skipHeaderLength){
            ++skip;                   //跳过开始skipHeaderLength行数
            QVector<QVariant> formData;
            QString s=this->stream->readLine();        //文件开始几行可能不是正文数据，无法确定这几行每行有几列数据，所以直接读一行出来

            formData.append(s);
            SkipedContents.push_back(formData);
           // qDebug()<<"跳过的数据:"<<formData;
        }
        else{
            QVector<QVariant> formData;
            QStringList list;
            for(int i=0;i<column_type.size();++i){         //循环结束时读完一行数据，
                QString s;
                (*stream)>>s;
                list.append(s);      //追加    循环结束时list中保存一行数据
                //qDebug()<<s;
            }
            qDebug()<<list;
            for(int i=0;i<list.size();++i){
                if(column_type.at(i)=="QString"){
                    formData.push_back(list.at(i));
                }
                if(column_type.at(i)=="int"){
                    int data=list.at(i).toInt();
                    formData.push_back(data);
                }
                if(column_type.at(i)=="double"){
                    double data=list.at(i).toDouble();
                    formData.push_back(data);
                }
            }
            this->Contents.push_back(formData);
            //qDebug()<<"正文数据:"<<formData;
        }
    }
    //qDebug()<<"file is been readed ";

}

bool FileUtil::setFileConfine(){
    qDebug()<<'\n'<<fileName;

    if(fileName=="BusVehBus.txt"){  //文件内容的格式不同，针对不同文件做处理
        this->skipLength=2;
        column_name.push_back("线路编号");
        column_type.push_back("QString");
        column_changeable.push_back(false);

        column_name.push_back("配车数");
        column_type.push_back("int");
        column_changeable.push_back(true);

        column_name.push_back("当量系数");
        column_type.push_back("int");
        column_changeable.push_back(true);

        column_name.push_back("发车间隔");
        column_type.push_back("int");
        column_changeable.push_back(true);

        column_name.push_back("容量");
        column_type.push_back("int");
        column_changeable.push_back(true);

        column_name.push_back("类型");
        column_type.push_back("int");
        column_changeable.push_back(true);

        column_name.push_back("设定速度");
        column_type.push_back("int");
        column_changeable.push_back(true);

        column_name.push_back("线路名称");
        column_type.push_back("QString");
        column_changeable.push_back(true);

        read(skipLength);
    }
    else if(fileName=="geom.txt"){
        skipLength=0;
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_type.push_back("int");
        column_changeable.push_back(false);

        column_type.push_back("double");
        column_changeable.push_back(false);

        column_type.push_back("int");
        column_changeable.push_back(false);

        column_type.push_back("int");
        column_changeable.push_back(false);

        column_type.push_back("double");
        column_changeable.push_back(false);

        column_type.push_back("double");
        column_changeable.push_back(false);

        column_type.push_back("int");
        column_changeable.push_back(false);

        column_type.push_back("QString");
        column_changeable.push_back(false);

        read(skipLength);
    }
    else if(fileName=="lparm.txt"){
        skipLength=1;
        column_type.push_back("int");  //第一行
        column_changeable.push_back(true);

        column_type.push_back("int");
        column_changeable.push_back(true);

        column_type.push_back("double");
        column_changeable.push_back(true);

        column_type.push_back("int");
        column_changeable.push_back(true);

        column_type.push_back("double");
        column_changeable.push_back(true);

        column_type.push_back("double");
        column_changeable.push_back(true);

        column_type.push_back("double");
        column_changeable.push_back(true);

        column_type.push_back("int");
        column_changeable.push_back(true);

        column_type.push_back("double");
        column_changeable.push_back(true);

        column_type.push_back("double");
        column_changeable.push_back(true);

        read(skipLength);
    }
   else if(fileName=="ManageR1.txt"){
        skipLength=1;
        column_name.push_back("起点");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("终点");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("自行车");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("小汽车");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("摩托车");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("步行");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("货车");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("公交");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("地铁");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("船只");
        column_type.push_back("int");
        column_changeable.push_back(false);

        read(skipLength);
    }else if(fileName=="conn.txt"){
        skipLength=1;
        column_name.append("节点编号");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.append("邻接点数");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.append("邻接点");
        column_type.push_back("QString");
        column_changeable.push_back(false);
        readConn();
    }else if(fileName=="coor.txt"){
        skipLength=0;
        column_name.append("节点编号");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.append("X");
        column_type.push_back("double");
        column_changeable.push_back(false);

        column_name.append("Y");
        column_type.push_back("double");
        column_changeable.push_back(false);

        read(skipLength);
    }else if(fileName=="cont.txt"){
        skipLength=0;
        column_name.append("节点编号");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.append("节点类型");
        column_type.push_back("int");
        column_changeable.push_back(false);

        read(skipLength);
    }else if(fileName=="chnn.txt"){
        skipLength=1;

        column_name.append("节点编号");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.append("小区编号");
        column_type.push_back("int");
        column_changeable.push_back(false);

        read(skipLength);
    }else if(fileName=="ManageN.txt"){
        skipLength=2;
        column_name.push_back("节点编号");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("限制转向的序列");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("form");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.append("via");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.append("to");
        column_type.push_back("int");
        column_changeable.push_back(false);

        read(skipLength);
    }else if(fileName=="ManageL.txt"){
        skipLength=1;

        column_name.append("起点");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.append("终点");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.append("公交专用道条数");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.append("路侧停车情况");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.append("绿波交通设置");
        column_type.push_back("int");
        column_changeable.push_back(false);
        read(skipLength);
    }else if(fileName=="ManageC.txt"){

        skipLength=1;

        column_name.append("起点");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.append("终点");

        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.append("基准费率");
        column_type.push_back("double");
        column_changeable.push_back(false);

        column_name.append("居民费率");
        column_type.push_back("int");
        column_changeable.push_back(false);

        read(skipLength);
    }else if(fileName=="ManageR2.txt"){
        skipLength=1;
        column_name.push_back("起点");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("终点");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("小汽车");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("摩托车");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("货车");
        column_type.push_back("int");
        column_changeable.push_back(false);

        read(skipLength);
    }else if(fileName=="BusStopBus.txt"||fileName=="BusStopBRT.txt"||fileName=="BusStopWaterway.txt"||fileName=="BusStopTrain.txt"){
        skipLength=2;

        column_name.push_back("线路编号");
        column_type.push_back("QString");
        column_changeable.push_back(false);

        column_name.push_back("站点数");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("所经站点");
        column_type.push_back("QString");
        column_changeable.push_back(false);

        readBusStopFile();
    }else if(fileName=="BusVehBus.txt"||fileName=="BusVehBRT.txt"){
        skipLength=2;

        column_name.push_back("线路编号");
        column_type.push_back("QString");
        column_changeable.push_back(false);

        column_name.push_back("配车数");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("当量");
        column_type.push_back("double");
        column_changeable.push_back(false);

        column_name.push_back("发车间隔");
        column_type.push_back("double");
        column_changeable.push_back(false);

        column_name.push_back("容量");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("类型");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("设定速度");
        column_type.push_back("double");
        column_changeable.push_back(false);

        column_name.push_back("线路名称");
        column_type.push_back("QString");
        column_changeable.push_back(false);

        read(skipLength);
    }else if(fileName=="BusVehTrain.txt"||fileName=="BusVehWaterway.txt"){
        skipLength=2;

        column_name.push_back("线路编号");
        column_type.push_back("QString");
        column_changeable.push_back(false);

        column_name.push_back("配车数");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("发车间隔");
        column_type.push_back("double");
        column_changeable.push_back(false);

        column_name.push_back("容量");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("类型");
        column_type.push_back("int");
        column_changeable.push_back(false);

        column_name.push_back("设定速度");
        column_type.push_back("double");
        column_changeable.push_back(false);

        column_name.push_back("线路名称");
        column_type.push_back("QString");
        column_changeable.push_back(false);

        read(skipLength);
    }
    else{
        QMessageBox::warning(NULL,"错误！","您选择了一个无效文件");
        return false;
    }
    return true;
}

bool FileUtil::openFile(QFile *f){
    if(f!=NULL){
        file=f;
        fileFullName=file->fileName();
        if(fileFullName.isEmpty()){
            qDebug()<<"当前文件名为空";
            return false;
        }
    }else{
        fileFullName=QFileDialog::getOpenFileName(NULL,"Open File","/home");
        if(fileFullName.isEmpty()){
            qDebug()<<"当前文件名为空";
            return false;
        }
        file=new QFile(fileFullName);
    }

    fileName=fileFullName.split('/').last();
    qDebug()<<'\n'<<"文件名："<<fileName;
    if (!file->open(QIODevice::ReadWrite | QIODevice::Text))
        return false;
    stream=new QTextStream(file);
    if(!setFileConfine()){      //设置表格操作限定信息和表头内容信息
        return false;
    }

    //读文件操作在setFileConfine函数内部，
    file->close();
    delete stream;
    return true;

}

QVector<QString>* FileUtil::getColumn_name()
{
    return &column_name;
}

QVector<QVector<QVariant>>* FileUtil::getContents()
{
    return &Contents;
}

QString FileUtil::getColumntype(int column) const
{
    return column_type.at(column);
}

QVector<bool> FileUtil::getColumn_changeable() const
{
    return column_changeable;
}

bool FileUtil::isEditable(int column){
    return column_changeable.at(column);
}

void FileUtil::setContentsData(int row, int column, QVariant data){
    if(row<Contents.size()){
        QVector<QVariant> &dataList=Contents[row];
        if(column<dataList.size()){
           dataList[column]=data;
        }else{
            QMessageBox::information(NULL,"错误！","下标越界！");
        }
    }
}

void FileUtil::saveFile(){
    qDebug()<<"saveFile is been invoked";
    if (!file->open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox::warning(NULL,"错误！","保存文件失败！");
        return;
    }
    QTextStream stream(file);
    if(!SkipedContents.isEmpty()){
        for(int i=0;i<SkipedContents.size();++i){
            QVector<QVariant> skipedData=SkipedContents.at(i);
            for(int j=0;j<skipedData.size();++j){

                stream<<skipedData.at(j).toString();

                if(j==skipedData.size()-1)     //最后一列输出换行
                    stream<<'\n';
                else{
                    stream<<'\t';       //不是最后一列输出空格
                }

            }
        }
    }
    if(!Contents.isEmpty()){
        for(int i=0;i<Contents.size();++i){
            QVector<QVariant> Data=Contents.at(i);
            for(int j=0;j<Data.size();++j){
                stream<<Data.at(j).toString();

                if(j==Data.size()-1)     //不是最后一行的最后一列输出换行
                {   if(i!=Contents.size()-1)
                        stream<<'\n';
                }
                else{
                    stream<<'\t';       //不是最后一列输出\t
                }
            }
        }
    }
    stream.flush();

    file->close();
}

QString FileUtil::getFileFullName() const
{
    return fileFullName;
}

void FileUtil::readConn(){
    int connSize = stream->readLine().toInt();       // 首先读取网络节点数
    QVector<QVariant> formData;
    formData.append(connSize);
    SkipedContents.push_back(formData);
    while(!stream->atEnd()) {
        int number; // 编号
        int count;  // 邻接边数量
        QString neiNodeList;
        QVector<QVariant> rowcontents;
        (*stream) >> number >> count;               //读入编号和领结边数量
        rowcontents.append(number);        //第一列
        rowcontents.append(count);     //第二列
        if (stream->status() == QTextStream::ReadPastEnd)
                break;
        for (int i = 0; i < count; i++) {
            int node;
            (*stream) >> node;
            neiNodeList.append(QString::number(node));
            if(i!=count-1){
                neiNodeList.append(",")  ;      //邻接点以英文逗号分隔，最后一个邻接点后不加逗号
            }
        }
        rowcontents.append(QVariant(neiNodeList));    //第三列
        Contents.append(rowcontents);          //追加一行
    }
}

void FileUtil::readBusStopFile(){
    QTextStream &in=(*stream);
    QString number;
    int count, nodeNumber;
    // 前两行是说明文字
    QString skipcontent=in.readLine();
    QVector<QVariant> formData;
    formData.append(QVariant(skipcontent));
    SkipedContents.append(formData);

    skipcontent=in.readLine();
    QVector<QVariant> formData1;
    formData1.append(QVariant(skipcontent));
    SkipedContents.append(formData1);
    // 从第三行开始读数据

    while (!in.atEnd()) {
        QString nodeList;
        QVector<QVariant> formData;
        in >> number >> count;       //读线路编号和站点数
        formData.append(number);   //第一列
        formData.append(count);      //第二列
        if(in.atEnd()){ // 解决行尾多读一行空数据的问题
            break;
        }
        for(int i=0; i<count; ++i){
            in >> nodeNumber;
            nodeList.append(QString::number(nodeNumber));
            if(i!=count-1)
                nodeList.append(",")    ;   //站点编号之间用英文逗号隔开
        }
        formData.append(QVariant(nodeList));   //第三列
        Contents.append(formData);      //保存一行数据
    }
}

//void FileUtil::readBusVehBusorBRT(){
//    QString number, lineName;
//    int busNum, capacity, bustype;
//    qreal pcu, interval, speed;
//    // 前两行是说明文字
//    in.readLine();
//    in.readLine();
//    // 从第三行开始读数据
//    QList<DataRecBusVeh> recList;
//    while (!in.atEnd()) {
//        in >> number ;
//        if(in.atEnd()){ // 处理行尾多度一行空数据的问题
//            break;
//        }
//        in>> busNum >> pcu >> interval >> capacity >> bustype >> speed >> lineName;
//        DataRecBusVeh rec(number, busNum, pcu, interval, capacity, bustype, speed, lineName);
//        recList << rec;
//    }
//}

