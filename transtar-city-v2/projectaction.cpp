﻿#include "projectaction.h"
#include <QDir>
#include <QFile>

ProjectAction::ProjectAction(QWidget *parent):QAction(parent)
{
    this->setStatusTip(tr("打开此项目"));
    connect(this,SIGNAL(triggered(bool)),this,SLOT(openProject()));

}

Project ProjectAction::getProject() const
{
    return project;
}

void ProjectAction::setProject(const Project &value)
{
    project = value;
}

void ProjectAction::openProject(){
    emit sendProject(project);
}
