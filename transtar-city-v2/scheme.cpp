﻿#include "scheme.h"

Scheme::Scheme()
{

}

QString Scheme::getSchemeName() const
{
    return schemeName;
}

void Scheme::setSchemeName(const QString &value)
{
    schemeName = value;
}

QString Scheme::getSchemeRelativePath() const
{
    return schemeRelativePath;
}

void Scheme::setSchemeRelativePath(const QString &value)
{
    schemeRelativePath = value;
}

bool Scheme::getIsBasic() const
{
    return isBasic;
}

void Scheme::setIsBasic(bool value)
{
    isBasic = value;
}

bool Scheme::getIsActive() const
{
    return isActive;
}

void Scheme::setIsActive(bool value)
{
    isActive = value;
}

QString Scheme::getSchemeId() const
{
    return schemeId;
}

void Scheme::setSchemeId(const QString &value)
{
    schemeId = value;
}

QString Scheme::getBasedOn() const
{
    return basedOn;
}

void Scheme::setBasedOn(const QString &value)
{
    basedOn = value;
}

QJsonObject Scheme::write(){

    QJsonObject scheme_json;
    scheme_json["scenario_id"]=schemeId;
    scheme_json["scenario_name"]=schemeName;
    scheme_json["relative_path"]=schemeRelativePath;
    scheme_json["is_active"]=isActive;
    scheme_json["is_basic"]=isBasic;
    scheme_json["based_on"]=basedOn;
    return scheme_json;
}

void Scheme::read(QJsonObject json){
    this->basedOn=json["based_on"].toString();
    this->isActive=json["is_active"].toBool();
    this->isBasic=json["is_basic"].toBool();
    this->schemeId=json["scenario_id"].toString();
    this->schemeRelativePath=json["relative_path"].toString();
    this->schemeName=json["scenario_name"].toString();
}
