﻿#include "helpbrowser.h"

HelpBrowser::HelpBrowser(QHelpEngine* helpEngine,
                         QWidget* parent):QTextBrowser(parent),
                         helpEngine(helpEngine)
{

}

QVariant HelpBrowser::loadResource(int type, const QUrl &name){    //覆盖此函数，才能正常显示帮助文档信息
    if (name.scheme() == "qthelp")
        return QVariant(helpEngine->fileData(name));
    else
        return QTextBrowser::loadResource(type, name);
}
