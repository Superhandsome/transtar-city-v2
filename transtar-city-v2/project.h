﻿#ifndef PROJECT_H
#define PROJECT_H
#include <QString>
#include <QObject>
#include <QVector>
#include "scheme.h"
#include <QJsonObject>

class Project
{
public:
    Project();
    int getId() const;
    void setId(int value);

    QString getProjectName() const;
    void setProjectName(const QString &value);

    QString getProjectPath() const;
    void setProjectPath(const QString &value);

    long getLastUsedTime() const;
    void setLastUsedTime(long value);
    QJsonObject write();   //写信息到json中
    void read(QJsonObject json);     //从Json中读书据

    QVector<Scheme> &getSchemeList();

    bool getIsFirstCreate() const;
    void setIsFirstCreate(bool value);
    void addScheme(Scheme scheme);
private:
    int id;
    QString projectName;
    QString projectPath;
    long createdTime;
    bool isFirstCreate;    //判断是否是第一次创建方案
    QVector<Scheme> schemeList;
};

#endif // PROJECT_H
