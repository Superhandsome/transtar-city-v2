﻿#include "projectdbnavigate.h"
#include "projectdbUtil.h"
#include <QSqlQuery>
#include <QVariant>
#include <QDebug>
#include <QVariant>
#include <QDateTime>

projectDbNavigate::projectDbNavigate()
{

}

QVector<Project> projectDbNavigate::selectProject(){

    if(ProjectDbUtil::createConnection()){
        qDebug()<<"创建数据库连接成功";

    }else{
        qDebug()<<"创建数据库连接失败";
        return QVector<Project>();
    }

    QSqlQuery query;
    if(!query.exec("SELECT * FROM projectInfo order by lastusedTime DESC")){   //按照时间降序
        qDebug()<<"执行数据库查询失败";
        return QVector<Project>();
    }
    int count=0;
    QVector<Project> projectList;
    while(query.next()&&count<10){                 //最多显示10条数据
        int id=query.value(0).toInt();
        QString projectName=query.value(1).toString();
        QString projectPath=query.value(2).toString();
        long createdTime=query.value(3).toLongLong();
        Project project;
        project.setId(id);
        project.setProjectName(projectName);
        project.setProjectPath(projectPath);
        project.setLastUsedTime(createdTime);
        projectList.append(project);
        count++;
    }
    return projectList;
}

void projectDbNavigate::insertProject(Project *project){
    qDebug()<<"调用插入数据库函数";
    if(ProjectDbUtil::createConnection()){
        qDebug()<<"创建数据库连接成功";

    }else{
        qDebug()<<"创建数据库连接失败";
        return;
    }
    QSqlQuery query;
    query.prepare("INSERT INTO projectInfo (projectName,projectPath,lastusedTime)"
                  "VALUES (:projectName,:projectPath,:lastusedTime)");
    query.bindValue(0, project->getProjectName());
    query.bindValue(1, project->getProjectPath());
    query.bindValue(2, QVariant(qlonglong(project->getLastUsedTime())));
    if(!query.exec()){
        qDebug()<<"插入项目数据失败";
    }else{
        qDebug()<<"插入项目数据成功";
    }
}

void projectDbNavigate::updataLastUsedTime(Project *project){
    qDebug()<<"调用更新最近使用时间函数";
    if(ProjectDbUtil::createConnection()){
        qDebug()<<"创建数据库连接成功";

    }else{
        qDebug()<<"创建数据库连接失败";
        return;
    }
    QSqlQuery query;
    QString commit=QString("update projectInfo set lastusedTime=%1 where projectName=\"%2\"")
            .arg(QString::number(QDateTime::currentDateTime().toTime_t()))
            .arg(project->getProjectName());

    bool isok=query.exec(commit);
    if(isok){
       // qDebug()<<"更新最近使用时间成功";
    }else{
        qDebug()<<commit;
        qDebug()<<"更新最近使用时间失败";
    }
}
