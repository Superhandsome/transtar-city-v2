﻿#ifndef FILESTATEWATCHER_H
#define FILESTATEWATCHER_H

//#include <QObject>
#include <QFile>
#include "project.h"
#include "scheme.h"
#include <QMap>

enum FileState{Normal=0,Losed=1,Destroyed=2};
enum FileEditState{Saved=0,UnSaved=1};

class FileStateWatcher
{
   // Q_OBJECT
public:
    static FileStateWatcher *getInstance();

    FileState getFileState(QString fileName);
    FileEditState getFileEditState(QString fileFullName);

    void setOpendProject(Project *project);     //监测软件状态，当软件当前打开的项目和活动方案改变时，状态机将收到通知
    void setActivedScheme(Scheme *scheme);
    Project *getProject();
    Scheme *getScheme();

    void setFileEditState(QString fileFullName,bool edited=true);
private:
    FileStateWatcher();

    static FileStateWatcher *watcher;

    Project *project;         //保存当前打开项目
    Scheme *scheme;               //当前项的当前活动方案

    QMap<QString,FileEditState> fileEditMap;             //保存当前方案下的数据文件是否被编辑
};

#endif // FILESTATEWATCHER_H
