﻿#ifndef CHOSEDATAWIDGET_H
#define CHOSEDATAWIDGET_H

#include <QWidget>
#include <QVector>
#include <QStandardItemModel>

namespace Ui {
class ChoseDataWidget;
}

class ChoseDataWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ChoseDataWidget(QWidget *parent = 0);
    ~ChoseDataWidget();
public slots:
    void dataTypeChanged(int index);
    void openFile(QModelIndex index);
signals:
    void openFileSignal(QString filename);
private:
    Ui::ChoseDataWidget *ui;
    void initData();
   QVector<QStandardItemModel *> modelList;

};

#endif // CHOSEDATAWIDGET_H
