﻿#include "transtarmainwindow.h"
#include "ui_transtarmainwindow.h"
#include <QDebug>
#include <QTreeView>
#include <QFileSystemModel>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QDir>
#include "projectaction.h"
#include "projectdbnavigate.h"
#include "projectutil.h"
#include <QFileDialog>
#include "newprojectdialog.h"
#include <QFont>
#include <QColor>
#include "newschemedialog.h"
#include <QMetaType>
#include "projectutil.h"
#include <QMessageBox>
#include <QDateTime>
#include "chosedatawidget.h"
#include <QFrame>
#include "helpwidget.h"
#include <QIcon>
#include "filestatewatcher.h"
#include <QCloseEvent>

#define Role_Project Qt::UserRole+4
#define Role_Scheme Qt::UserRole+5
#define Role_Scheme_Basic Qt::UserRole+6  //定义自定义角色
#define Role_Scheme_Active Qt::UserRole+7
#define Scheme_ID Qt::UserRole+8

#define Mark_Project 1               //定义角色对应的data值
#define Mark_Scheme 2
#define Mark_Scheme_Basic 3
#define Mark_Scheme_Active 4

TranstarMainWindow::TranstarMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TranstarMainWindow)
{
    ui->setupUi(this);

    initWinFramework();
    showMaximized(); // 最大化显示
    model=NULL;
    view=NULL;
    mTabWidget=0;
    //tabWindow=0;
    this->dataWindow=0;
    fileIndex=-1;           //设置初始状态
    file=NULL;
    setContextMenuPolicy(Qt::CustomContextMenu);
    initContentMenu();   //设置view的上下文菜单
    initRecentProject();      //设置最近项目，添加三级菜单

    connect(ui->openprojectAction,SIGNAL(triggered(bool)),this,SLOT(openProject()));
    connect(ui->newprojectAction,SIGNAL(triggered(bool)),this,SLOT(newProject()));
    connect(ui->showDataAction,SIGNAL(triggered(bool)),this,SLOT(initChoseDataWidget()));
    connect(ui->helpaction,SIGNAL(triggered(bool)),this,SLOT(showHelpWidget()));
}

TranstarMainWindow::~TranstarMainWindow()
{
    delete ui;
}

Project TranstarMainWindow::getProject() const
{
    return project;
}

void TranstarMainWindow::setProject(Project value)
{
    project = value;
    FileStateWatcher::getInstance()->setOpendProject(&project);    //状态机中保存当前打开项目；这是一个软件状态
    if(model!=NULL){
        delete model;
    }
    if(view!=NULL){
        delete view;
    }
    initProjectTreeView();
}


void TranstarMainWindow::initTabWidget()
{

    mTabWidget = new QTabWidget(this);
    mTabWidget->setTabPosition(QTabWidget::South);
    mTabWidget->setTabsClosable(true);

    connect(mTabWidget, SIGNAL(tabCloseRequested(int)), this, SLOT(closeTabWin(int)));
    setCentralWidget(mTabWidget);
}

void TranstarMainWindow::closeTabWin(int index)  //
{
    qDebug() << "tab widget close request" << endl;
//    if(mTabWidget->widget(index)==mGraphicsWin){
//        bool gwClosable = mGraphicsWin->checkClose();
//        if(gwClosable){
//            mTabWidget->removeTab(index);
//        }
//    }else{
//        mTabWidget->removeTab(index);
//    }
//    QCloseEvent *event=new QCloseEvent();
//    mTabWidget->widget(index);
    if(fileIndex==index){     //当前所要关闭的窗口是显示数据文件的DataWindow

        saveFile();
        fileIndex=-1;
        dataWindow=0;
        file=NULL;
    }

    mTabWidget->removeTab(index);

}

void TranstarMainWindow::initWinFramework()
{
    // 设置角落
    setCorner(Qt::TopLeftCorner, Qt::LeftDockWidgetArea);
    setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
    setCorner(Qt::TopRightCorner, Qt::RightDockWidgetArea);
    setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);

    // 5个dock锚点
    mLeftAnchorDock = new QDockWidget("");
    mLeftAnchorDock->setWidget(new QWidget());
    mLeftAnchorDock->setVisible(false);
    addDockWidget(Qt::LeftDockWidgetArea, mLeftAnchorDock);
    mLeftBottomAnchorDock = new QDockWidget("");
    mLeftBottomAnchorDock->setWidget(new QWidget());
    mLeftBottomAnchorDock->hide();
    addDockWidget(Qt::LeftDockWidgetArea, mLeftBottomAnchorDock);
    mBottomAnchorDock = new QDockWidget("");
    mBottomAnchorDock->setWidget(new QWidget());
    mBottomAnchorDock->hide();
    addDockWidget(Qt::BottomDockWidgetArea, mBottomAnchorDock);
    mRightAnchorDock = new QDockWidget("");
    mRightAnchorDock->setWidget(new QWidget());
    mRightAnchorDock->hide();
    addDockWidget(Qt::RightDockWidgetArea, mRightAnchorDock);
    mRightBottomAnchorDock = new QDockWidget("");
    mRightBottomAnchorDock->setWidget(new QWidget());
    mRightBottomAnchorDock->hide();
    addDockWidget(Qt::RightDockWidgetArea, mRightBottomAnchorDock);
}

void TranstarMainWindow::addDockToLeft(QDockWidget *dock, bool show)
{
    addDockWidget(Qt::LeftDockWidgetArea, dock);
    tabifyDockWidget(mLeftAnchorDock, dock);
    if(show){ // 这是目前发现的，直接显示新的dock的最好方式，只用tabifyDockWidget()不行
        dock->show();
        dock->raise();
    }else{
        dock->close();
    }
}

void TranstarMainWindow::addDockToLeftBottom(QDockWidget *dock, bool show)
{
    addDockWidget(Qt::LeftDockWidgetArea, dock);
    tabifyDockWidget(mLeftBottomAnchorDock, dock);
    if(show){ // 这是目前发现的，直接显示新的dock的最好方式，只用tabifyDockWidget()不行
        dock->show();
        dock->raise();
    }else{
        dock->close();
    }
}

void TranstarMainWindow::addDockToBottom(QDockWidget *dock, bool show)
{
    addDockWidget(Qt::BottomDockWidgetArea, dock);
    tabifyDockWidget(mBottomAnchorDock, dock);
    if(show){ // 这是目前发现的，直接显示新的dock的最好方式，只用tabifyDockWidget()不行
        dock->show();
        dock->raise();
    }else{
        dock->close();
    }
}

void TranstarMainWindow::addDockToRight(QDockWidget *dock, bool show)
{
    addDockWidget(Qt::RightDockWidgetArea, dock);
    tabifyDockWidget(mRightAnchorDock, dock);
    if(show){ // 这是目前发现的，直接显示新的dock的最好方式，只用tabifyDockWidget()不行
        dock->show();
        dock->raise();
    }else{
        dock->close();
    }
}

void TranstarMainWindow::addDockToRightBottom(QDockWidget *dock, bool show)
{
    addDockWidget(Qt::RightDockWidgetArea, dock);
    tabifyDockWidget(mRightBottomAnchorDock, dock);
    if(show){ // 这是目前发现的，直接显示新的dock的最好方式，只用tabifyDockWidget()不行
        dock->show();
        dock->raise();
    }else{
        dock->close();
    }
}

int TranstarMainWindow::addWinToCenter(QWidget* widget, QString tabName, bool show)
{
    if(mTabWidget==0){
        initTabWidget();
    }
    int index=mTabWidget->addTab(widget, tabName);
    if(show){
        mTabWidget->setCurrentWidget(widget);
    }
    return index;           //返回添加的窗口的index
}

void TranstarMainWindow::initProjectTreeView(){

    qRegisterMetaType<Scheme>("Scheme");
    model=new QStandardItemModel;
    model->setHorizontalHeaderLabels(QStringList()<<QStringLiteral("项目"));
    QStandardItem *root=new QStandardItem;
    root->setData(Mark_Project,Role_Project);
    root->setData(project.getProjectName(),Qt::DisplayRole);

    model->appendRow(root);
    QVector<Scheme> &schemeList=project.getSchemeList();
    for(int i=0;i<schemeList.size();++i){
        Scheme scheme=schemeList[i];
        QStandardItem *schemeItem=new QStandardItem;
        schemeItem->setData(Mark_Scheme,Role_Scheme);
        schemeItem->setData(scheme.getSchemeName(),Qt::DisplayRole);
        schemeItem->setData(scheme.getSchemeId(),Scheme_ID);   //保存scheme对象的ID
        if(scheme.getIsActive()){
            activeScheme=scheme;
            FileStateWatcher::getInstance()->setActivedScheme(&activeScheme);      //状态机中保存了当前活动方案，这个软件状态
            qDebug()<<"当前的活动方案是"<<activeScheme.getSchemeName();
            active_item=schemeItem;
            schemeItem->setData(Mark_Scheme_Active,Role_Scheme_Active);
            QFont font=qApp->font();
            font.setWeight(QFont::Bold);
            schemeItem->setData(font,Qt::FontRole);   //设置活动方案字体加粗
            //schemeItem->setData(Qt::cyan,Qt::BackgroundRole);
        }
        if(scheme.getIsBasic()){
            basic_item=schemeItem;
            schemeItem->setData(Mark_Scheme_Basic,Role_Scheme_Basic);
            //schemeItem->setData(Qt::darkBlue,Qt::BackgroundRole);
        }
        root->appendRow(schemeItem);
    }
    view=new QTreeView;
    view->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(view,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(setContents(QPoint)));
    view->setModel(model);
   view->setEditTriggers(QAbstractItemView::NoEditTriggers);
    mLeftAnchorDock->setWidget(view);
    mLeftAnchorDock->show();
    qDebug()<<project.getProjectName();
}

void TranstarMainWindow::setContents(QPoint point){
    this->point=point;  //按钮位置赋值给类成员变量point，后面的一些action需要根据位置获得index，来操作project
    QModelIndex index=view->indexAt(point);
    QVariant data=model->data(index,Role_Project);
    if(data.isValid()){
        if(data.toInt()==Mark_Project){
            project_Menu->exec(QCursor::pos());  //弹出右键菜单，菜单位置为鼠标光标位置
        }
    }
    data=model->data(index,Role_Scheme);
    if(data.isValid()){
        if(data.toInt()==Mark_Scheme){
            scheme_Menu->exec(QCursor::pos());
        }
    }
}

void TranstarMainWindow::initRecentProject(){
    if(ui->recentprojectMenu->actions().size()!=0){
        ui->recentprojectMenu->clear();             //清空原来的三级菜单，每次新建项目之后更新最近项目显示
    }
    QVector<Project> projectList=projectDbNavigate::selectProject();

    qDebug()<<"子菜单";
    for(int i=0;i<projectList.size();++i){
        ProjectAction *action=new ProjectAction;
        action->setProject(projectList.at(i));
        action->setText(projectList.at(i).getProjectName());
        connect(action,SIGNAL(sendProject(Project)),this,SLOT(openRecentProject(Project)));
        ui->recentprojectMenu->addAction(action);

    }
}

void TranstarMainWindow::initContentMenu(){
    project_Menu=new QMenu;
    QAction *new_scheme=new QAction(tr("新建方案"));
    connect(new_scheme,SIGNAL(triggered(bool)),this,SLOT(newScheme()));
    project_Menu->addAction(new_scheme);

    QAction *close_project=new QAction(tr("关闭项目"));
    connect(close_project,SIGNAL(triggered(bool)),this,SLOT(closeProject()));
    project_Menu->addAction(close_project);

    scheme_Menu=new QMenu;
    QAction *setActive=new QAction(tr("设为活动方案"));
    connect(setActive,SIGNAL(triggered(bool)),this,SLOT(setSchemeisActive()));
    scheme_Menu->addAction(setActive);

    QAction *setBasic=new QAction(tr("设为基础方案"));
    connect(setBasic,SIGNAL(triggered(bool)),this,SLOT(setSchemeisBasic()));
    scheme_Menu->addAction(setBasic);

    QAction *deleteScheme=new QAction(tr("删除方案"));
    connect(deleteScheme,SIGNAL(triggered(bool)),this,SLOT(deleteScheme()));
    scheme_Menu->addAction(deleteScheme);
}

void TranstarMainWindow::newProject(){
    dialog=new NewProjectDialog(this);
    connect(dialog,SIGNAL(nextClicked(Project)),this,SLOT(createNewSchemeDialog(Project)));
    dialog->exec();

}

void TranstarMainWindow::newScheme(){
    //qDebug()<<"项目名称是"<<project->getProjectName();
    NewSchemeDialog *dialog=new NewSchemeDialog(&project,this);
    dialog->exec();

}

void TranstarMainWindow::setSchemeisActive(){      //设置显示为活动方案

    QModelIndex index=view->indexAt(point);
    QVector<Scheme> &schemeList=project.getSchemeList();
    QString scheme_ID=model->data(index,Scheme_ID).toString();
    for(int i=0;i<schemeList.size();++i){
        if(schemeList[i].getIsActive()){
            schemeList[i].setIsActive(false);
        }
        if(schemeList[i].getSchemeId()==scheme_ID){
            schemeList[i].setIsActive(true);
            activeScheme=schemeList[i];
            FileStateWatcher::getInstance()->setActivedScheme(&activeScheme);      //状态机中保存了当前活动方案，这个软件状态
            qDebug()<<schemeList[i].getSchemeName()<<schemeList[i].getIsActive();
        }
    }
    QString projectPath=project.getProjectPath();
    QDir dir(projectPath);
    dir.cd(project.getProjectName());    //进入项目文件夹
    QFile file(dir.filePath(project.getProjectName()+QString(".transpro")));    //打开项目文件
    if(!file.open(QIODevice::WriteOnly|QIODevice::Text)){
        qDebug()<<"打开项目文件失败";
        return;
    }
    ProjectUtil::saveProject(&project,&file);

    this->setProject(project);
    view->expandAll();
}

void TranstarMainWindow::setSchemeisBasic(){    //设置显示为基础方案，当前基础活动显示没有什么不一样的地方
   // QModelIndex index=view->indexAt(point);
    QModelIndex index=view->indexAt(point);
    QVector<Scheme> &schemeList=project.getSchemeList();
    QString scheme_ID=model->data(index,Scheme_ID).toString();
    for(int i=0;i<schemeList.size();++i){
        if(schemeList[i].getIsBasic()){
            schemeList[i].setIsBasic(false);
        }
        if(schemeList[i].getSchemeId()==scheme_ID){
            schemeList[i].setIsBasic(true);
            qDebug()<<schemeList[i].getSchemeName()<<schemeList[i].getIsBasic();
        }
    }
    QString projectPath=project.getProjectPath();
    QDir dir(projectPath);
    dir.cd(project.getProjectName());    //进入项目文件夹
    QFile file(dir.filePath(project.getProjectName()+QString(".transpro")));    //打开项目文件
    if(!file.open(QIODevice::WriteOnly|QIODevice::Text)){
        qDebug()<<"打开项目文件失败";
        return;
    }
    ProjectUtil::saveProject(&project,&file);

    this->setProject(project);
    view->expandAll();
}

void TranstarMainWindow::openProject(){
    if(mLeftAnchorDock->isHidden()){
        mLeftAnchorDock->show();
    }
    qDebug()<<"打开项目";
    //弹出文件选择框，打开项目
    QString fileName=QFileDialog::getOpenFileName(this,"选择项目",QCoreApplication::applicationDirPath(),"Files(*.transpro)");
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly|QIODevice::Text)){
        qDebug()<<"打开项目文件失败";
        return ;
    }
    Project project;

    ProjectUtil::openProject(&project,&file);
    QDir dir(fileName);
    dir.cdUp();
    qDebug()<<"当前目录是"<<dir.currentPath();
    project.setProjectPath(dir.currentPath());
    project.setLastUsedTime(QDateTime::currentDateTime().toTime_t());
    QVector<Project> projectList=projectDbNavigate::selectProject();
    bool find_tag=false;
    for(int i=0;i<projectList.size();++i){
        if(projectList.at(i).getId()==project.getId()){
            find_tag=true;
            break;
        }
    }
    if(find_tag==false){    //此项目的记录不在数据库中时，插入数据库中
        projectDbNavigate::insertProject(&project);
    }
    this->setProject(project);
    FileStateWatcher::getInstance()->setOpendProject(&project);    //状态机中保存当前打开项目；这是一个软件状态
    qDebug()<<project.getProjectName();
    initRecentProject();    //更新最近项目显示
}

void TranstarMainWindow::closeProject(){

    view->close();
    mLeftAnchorDock->close();
}

void TranstarMainWindow::openRecentProject(Project project){
    QDir dir(project.getProjectPath());
    dir.cd(project.getProjectName());

    QFile file(dir.filePath(project.getProjectName()+QString(".transpro")));
    if(!file.open(QIODevice::ReadOnly|QIODevice::Text)){
        qDebug()<<"创建项目文件失败";
        return;
    }

    ProjectUtil::openProject(&project,&file);
    qDebug()<<project.getSchemeList().size();
    this->setProject(project);
    //qDebug()<<project.getSchemeList().size();

    initRecentProject();
}

void TranstarMainWindow::deleteScheme(){
    QModelIndex index=view->indexAt(point);
    QVector<Scheme> &schemeList=project.getSchemeList();  //引用
    QString scheme_ID=model->data(index,Scheme_ID).toString();
    int i;
    Scheme deleteScheme;
    for(i=0;i<schemeList.size();++i){
        if(schemeList[i].getSchemeId()==scheme_ID){
//            schemeList[i].setIsBasic(true);
//            qDebug()<<schemeList[i].getSchemeName()<<schemeList[i].getIsBasic();
           deleteScheme=schemeList.at(i);
           if(deleteScheme.getIsActive()||deleteScheme.getIsBasic()){
               QMessageBox::StandardButton button=QMessageBox::information(this,"提醒！","此项目是当前活动项目或基础项目！确定删除？",QMessageBox::Ok,QMessageBox::Cancel);
               if(button==QMessageBox::Cancel){
                   return;
               }
           }
            schemeList.remove(i);       //从改项目的方案列表中删除
            if(deleteScheme.getIsActive()){
                schemeList[qrand()%schemeList.size()].setIsActive(true);     //当删除活动方案时，随机选一个方案作为新的活动方案
            }
            if(deleteScheme.getIsBasic()){
                schemeList[qrand()%schemeList.size()].setIsBasic(true);   //当删除基础方案时，随机选一个方案作为新的基础方案
            }
        }
    }

    QString projectPath=project.getProjectPath();
    QDir dir(projectPath);
    dir.cd(project.getProjectName());    //进入项目文件夹
    QFile file(dir.filePath(project.getProjectName()+QString(".transpro")));    //打开项目文件
    if(!file.open(QIODevice::WriteOnly|QIODevice::Text)){
        qDebug()<<"打开项目文件失败";
        return;
    }
    ProjectUtil::saveProject(&project,&file);
    QString schemePath=project.getProjectPath()+"/"+project.getProjectName()+deleteScheme.getSchemeRelativePath();
    qDebug()<<schemePath;
    DeleteDirectory(schemePath);       //删除方案的文件夹
    this->setProject(project);
    view->expandAll();

}

bool TranstarMainWindow::DeleteDirectory(const QString &path)
{
    if (path.isEmpty())
        return false;

    QDir dir(path);
    if(!dir.exists())
        return true;

    dir.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot);
    QFileInfoList fileList = dir.entryInfoList();
    foreach (QFileInfo fi, fileList)
    {
        if (fi.isFile())
            fi.dir().remove(fi.fileName());
        else
            DeleteDirectory(fi.absoluteFilePath());
    }
    return dir.rmpath(dir.absolutePath());
}

void TranstarMainWindow::createNewSchemeDialog(Project pro){
    project=pro;
    FileStateWatcher::getInstance()->setOpendProject(&project);     //状态机中保存了当前活动方案，这个软件状态
    NewSchemeDialog *schemeDialog=new NewSchemeDialog(&project,this);
    connect(schemeDialog,SIGNAL(createSucceed()),this,SLOT(closeAndUpdate()));
    schemeDialog->exec();
    schemeDialog->close();

}
void TranstarMainWindow::closeAndUpdate(){

    //dialog->deleteLater();

    dialog->close();
    this->setProject(project);
    qDebug()<<project.getSchemeList().size();
    initRecentProject();
}

void TranstarMainWindow::initChoseDataWidget(){    //显示出数据列表浮动窗口
   // QFrame *frame=new QFrame;
    ChoseDataWidget *choseDataWidget=new ChoseDataWidget;
    connect(choseDataWidget,SIGNAL(openFileSignal(QString)),this,SLOT(openDataFile(QString)));   //连接打开文件槽

    mRightAnchorDock->setWidget(choseDataWidget);
    if(mRightAnchorDock->isHidden()){
        mRightAnchorDock->show();
    }

}

void TranstarMainWindow::showHelpWidget(){      //打开帮助窗口
    HelpWidget *helpWidget=new HelpWidget;
    this->addWinToCenter(helpWidget,"帮助");
}



void TranstarMainWindow::openDataFile(QString fileName){
    QString schemeName=activeScheme.getSchemeName();
    if(fileName.isEmpty()){
        QMessageBox::information(this,"文件异常","您没有指定有效文件");
        return;
    }
    if(FileStateWatcher::getInstance()->getFileState(fileName)==FileState::Losed){
        QMessageBox::warning(this,"文件异常",QString("%1 %2 文件丢失").arg(schemeName).arg(fileName));
        return;
    }
    if(FileStateWatcher::getInstance()->getFileState(fileName)==FileState::Destroyed){
        QMessageBox::warning(this,"文件异常",QString("%1 %2 文件被破坏").arg(schemeName).arg(fileName));
        return;
    }
    QString fileFullName=project.getProjectPath()+"/"+project.getProjectName()+activeScheme.getSchemeRelativePath()+"/data"+"/"+fileName;
    if(file!=NULL)
        if(fileFullName==file->fileName()){
            QMessageBox::information(this,"提示！",QString("%1文件已打开").arg(fileName));
            return;
        }
    file=new QFile(fileFullName);
   // qDebug()<<"文件的绝对路径名是"<<file->fileName();
    if(dataWindow==0){
        dataWindow=new DataWindow;
    }
    dataWindow->openFile(file);
    if(fileIndex==-1)       //当前没有显示数据文件的窗口
        fileIndex=addWinToCenter(dataWindow,fileName);
}

void TranstarMainWindow::saveFile(){
    if(FileStateWatcher::getInstance()->getFileEditState(file->fileName())==FileEditState::UnSaved){
        int tag=QMessageBox::warning(this," 文件未保存","是否保存文件？",QMessageBox::Save|QMessageBox::Cancel);
        if(tag==QMessageBox::Save){
            dataWindow->saveFile();
            FileStateWatcher::getInstance()->setFileEditState(file->fileName(),false);     //设置修改已保存，文件为未编辑状态

        }
    }
}

void TranstarMainWindow::closeEvent(QCloseEvent *event){
    QMainWindow::closeEvent(event);
    saveFile();
}
