﻿#include "lineeditdelegate.h"
#include <QLineEdit>

LineEditDelegate::LineEditDelegate(QObject *parent):QStyledItemDelegate(parent)
{

}

QWidget *LineEditDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const{

    QLineEdit *edit=new QLineEdit(parent);
   // connect(edit,SIGNAL(editingFinished()),
    return edit;

}

void LineEditDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const{
    QString text=index.model()->data(index,Qt::DisplayRole).toString();
    QLineEdit *edit=qobject_cast<QLineEdit*>(editor);
    edit->setText(text);
}

void LineEditDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const{
    QLineEdit *edit=qobject_cast<QLineEdit*>(editor);
    QString data=edit->text();
    model->setData(index,data,Qt::EditRole);
}

void LineEditDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const{
    editor->setGeometry(option.rect);
}
