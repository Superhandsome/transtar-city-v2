﻿#include "projectdbUtil.h"
#include <QDebug>
#include <QSqlQuery>

ProjectDbUtil::ProjectDbUtil()
{

}
bool ProjectDbUtil::createConnection(){
    QSqlDatabase db=QSqlDatabase::database();
    if(db.databaseName().isEmpty()){   //数据库连接名为空，说明没有打开数据库
        QSqlDatabase db=QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName("city-project");

        if(db.open()){
            qDebug()<<"打开数据库成功";

        }else{
            qDebug()<<"打开数据库失败";
            return false;
        }
        QSqlQuery query;
        query.exec(" CREATE TABLE projectInfo (  projectId    INTEGER PRIMARY KEY AUTOINCREMENT,projectName  TEXT    NOT NULL UNIQUE,projectPath  TEXT    NOT NULL, lastusedTime INTEGER NOT NULL  )" );
//        if(ok){
//            qDebug()<<"创建数据库表成功";
//            return true;
//        }else{
//            qDebug()<<"创建数据库表失败";
//            return false;
//        }
        return true;
    }else{       //数据库连接名不为空，说明已经打开数据库
        return true;
    }

}
