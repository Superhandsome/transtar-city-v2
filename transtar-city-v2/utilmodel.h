﻿#ifndef UTILMODEL_H
#define UTILMODEL_H

#include <QAbstractTableModel>
#include <QVector>
#include <QStringList>
#include <fileutil.h>
#include <QProgressDialog>
#include <QMap>

class UtilModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit UtilModel(QObject *parent = 0);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void setHorizontalHeader(QVector<QString> *value);

    void setFileData(QVector<QVector<QVariant>> *value);

    void setFileUtil(FileUtil *value);
    void printData();      //测试所用函数
    void printHeader();     //测试所用函数

    bool setData(const QModelIndex &index, const QVariant &value, int role); //设置可编辑
    Qt::ItemFlags flags(const QModelIndex &index) const;

    QMap<int,int>* search(QString s);
    QMap<int ,int> *result;
    bool isModifyed();
    int selectedItemRow;
    int selectedItemColumn;
    void setModifyTag(bool value);

private:

    int count;
    QVector<QString> *horizontalHeader;
    QVector<QVector<QVariant>> *fileData;

    FileUtil *fileUtil;

    QProgressDialog *progressDialog;
    bool modifyTag;

private slots:
    void changeProgressDialogNum();
signals:
    void readDataNowAt() const;
};

#endif // UTILMODEL_H
