﻿#include "chosedatawidget.h"
#include "ui_chosedatawidget.h"
#include <QStandardItem>
#include <QStandardItemModel>
#include "scheme.h"
#include "filestatewatcher.h"
#include "project.h"

#define FileRole Qt::UserRole+4

ChoseDataWidget::ChoseDataWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ChoseDataWidget)
{
    ui->setupUi(this);
    initData();
    ui->listView->setEditTriggers(QAbstractItemView::NoEditTriggers);   //设置不可编辑
    connect(ui->comboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(dataTypeChanged(int)));
    connect(ui->listView,SIGNAL(clicked(QModelIndex)),this,SLOT(openFile(QModelIndex)));
}

ChoseDataWidget::~ChoseDataWidget()
{
    delete ui;
}

void ChoseDataWidget::initData(){
    QStringList list;
    list<<"道路网络信息"<<"交通管理信息"<<"公共交通网络"<<"交通需求信息"<<"交通小区";
    ui->comboBox->addItems(list);
//    QStandardItemModel *model=new QStandardItemModel;
//    QStandardItem *item1=new QStandardItem;
//    item1->setData("文件1",Qt::DisplayRole);
//    model->appendRow(item1);
//    QStandardItem *item2=new QStandardItem;
//    item2->setData("文件2",Qt::DisplayRole);
//    model->appendRow(item2);
//    ui->listView->setModel(model);

   QStandardItemModel *model=new QStandardItemModel;
   QStandardItem *item=new QStandardItem;
   item->setData("网络连接表",Qt::DisplayRole);
   item->setData("conn.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("几何要素表",Qt::DisplayRole);
   item->setData("geom.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("坐标系统表",Qt::DisplayRole);
   item->setData("coor.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("节点类型表",Qt::DisplayRole);
   item->setData("cont.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("交通区节点转换表",Qt::DisplayRole);
   item->setData("chnn.txt",FileRole);
   model->appendRow(item);

   modelList.append(model);

   model=new QStandardItemModel;

   item=new QStandardItem;
   item->setData("节点交通管理信息",Qt::DisplayRole);
   item->setData("ManageN.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("线路交通管理信息",Qt::DisplayRole);
   item->setData("ManageL.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("区域禁止通行管理信息",Qt::DisplayRole);
   item->setData("ManageR1.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("区域限号通行管理信息",Qt::DisplayRole);
   item->setData("ManageR2.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("道路拥堵收费信息",Qt::DisplayRole);
   item->setData("ManageC.txt",FileRole);
   model->appendRow(item);

   modelList.append(model);

   model=new QStandardItemModel;

   item=new QStandardItem;
   item->setData("普通公交线路所经节点信息t",Qt::DisplayRole);
   item->setData("BusStopBus.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("普通公交线路车辆配置信息",Qt::DisplayRole);
   item->setData("BusVehBus.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("快速公交线路所经节点信息",Qt::DisplayRole);
   item->setData("BusStopBRT.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("快速公交线路车辆配置信息",Qt::DisplayRole);
   item->setData("BusVehBRT.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("轨道交通线路所经节点信息",Qt::DisplayRole);
   item->setData("BusStopTrain.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("轨道交通线路车辆配置信息",Qt::DisplayRole);
   item->setData("BusVehTrain.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("水运航道线路所经节点信息",Qt::DisplayRole);
   item->setData("BusStopWaterway.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("水运航道线路船只配置信息",Qt::DisplayRole);
   item->setData("BusVehWaterway.txt",FileRole);
   model->appendRow(item);

   modelList.append(model);

   model=new QStandardItemModel;
   item=new QStandardItem;
   item->setData("交通小区常住人口信息表",Qt::DisplayRole);
   item->setData("TripPopulaFloat.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("出行目的结构信息",Qt::DisplayRole);
   item->setData("TripPuse.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("城市居民职业结构信息",Qt::DisplayRole);
   item->setData("TripJob.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("交通小区流动人口信息表",Qt::DisplayRole);
   item->setData("TripPopulaFloat.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("交通小区土地利用信息表",Qt::DisplayRole);
   item->setData("LandUse.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("土地类型利用强度信息表",Qt::DisplayRole);
   item->setData("LanduseStrength.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("交通小区区位权重信息表",Qt::DisplayRole);
   item->setData("LandZoneWeight.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("工业用地货运量信息表",Qt::DisplayRole);
   item->setData("LanduseIndustrial.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("商业用地货运量信息表",Qt::DisplayRole);
   item->setData("LanduseCommercial.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("交通设施用地货运量信息表",Qt::DisplayRole);
   item->setData("LanduseTraffic.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("物流仓储用地货运量信息表",Qt::DisplayRole);
   item->setData("LanduseLogistics.txt",FileRole);
   model->appendRow(item);
   modelList.append(model);

  model=new QStandardItemModel;

   item=new QStandardItem;
   item->setData("交通小区边界数据表",Qt::DisplayRole);
   item->setData("Border.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("交通小区节点数据表",Qt::DisplayRole);
   item->setData("Cnode.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("交通区合并信息表",Qt::DisplayRole);
   item->setData("Odcombin.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("交通区拆分信息表",Qt::DisplayRole);
   item->setData("Oddivide.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("节点统计范围确认表",Qt::DisplayRole);
   item->setData("Rtin.txt",FileRole);
   model->appendRow(item);

   item=new QStandardItem;
   item->setData("路段统计范围确认表",Qt::DisplayRole);
   item->setData("Rtil.txt",FileRole);
   model->appendRow(item);
   modelList.append(model);

   dataTypeChanged(0);     //打开的时候显示第一类数据文件
}

void ChoseDataWidget::dataTypeChanged(int index){

    ui->listView->setModel(modelList[index]);
}

void ChoseDataWidget::openFile(QModelIndex index){
    int type=ui->comboBox->currentIndex();
    QStandardItemModel *model=modelList[type];
    QVariant data=model->data(index,FileRole);
    //qDebug()<<data.toString();
    emit openFileSignal(data.toString());       //发送打开文件信号
}
