﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <utilmodel.h>
#include <QFile>
#include <stdlib.h>
#include <QTextStream>
#include <fileutil.h>
#include <QLabel>
#include <QAction>
#include <QToolBar>

namespace Ui {
class DataWindow;
}

class DataWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit DataWindow(QWidget *parent = 0);
    ~DataWindow();
    void closeEvent(QCloseEvent *event);
public slots:
    void openFile(QFile *file=NULL);
    void saveFile();
    void search();

    void closeFileOpend();
private:
    void buildToolBar();

    Ui::DataWindow *ui;
    UtilModel *model;
    FileUtil *fileutil;
    QLabel *stateLabel;

    QAction *saveAction;
    QAction *searchAction;
    QAction *closeAction;

    QToolBar *commonToolBar;
};

#endif // MAINWINDOW_H
