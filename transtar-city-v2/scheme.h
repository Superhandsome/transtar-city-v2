﻿#ifndef SCHEME_H
#define SCHEME_H
#include <QString>
#include <QJsonObject>

class Scheme
{
public:
    Scheme();
    QString getSchemeName() const;
    void setSchemeName(const QString &value);

    QString getSchemeRelativePath() const;
    void setSchemeRelativePath(const QString &value);

    bool getIsBasic() const;
    void setIsBasic(bool value);

    bool getIsActive() const;
    void setIsActive(bool value);

    QString getSchemeId() const;
    void setSchemeId(const QString &value);

    QString getBasedOn() const;
    void setBasedOn(const QString &value);
    QJsonObject write();
    void read(QJsonObject json);
private:
    QString schemeName;
    QString schemeRelativePath;   //相对于项目的相对路径
    bool isBasic;     //是否是基本方案
    bool isActive;    //是否是活动方案
    QString schemeId;
    QString basedOn;  //如果基于其他活动，此项为基本活动的ID，基本活动此项类容为空
};

#endif // SCHEME_H
