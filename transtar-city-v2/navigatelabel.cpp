﻿#include "navigatelabel.h"
#include <QDebug>
#include <QMouseEvent>
#include <QSqlDatabase>
#include "newprojectdialog.h"
#include <QFileDialog>
#include "projectutil.h"
#include "transtarmainwindow.h"
#include <QCoreApplication>

NavigateLabel::NavigateLabel(QString text, QWidget *parent):QLabel(parent)
{
    setText(text);
}

NavigateType NavigateLabel::getType() const
{
    return type;
}

void NavigateLabel::setType(const NavigateType &value)
{
    type = value;
}

void NavigateLabel::mousePressEvent(QMouseEvent *event){
    if(event->button()==Qt::LeftButton){
        if(type==NavigateType::CREATE){
            emit createSignal();

        }else{
            emit openSignal();

        }
    }

}
