﻿#include "datawindow.h"
#include "ui_datawindow.h"
#include <QFileDialog>
#include <QDebug>
#include <QStandardItemModel>
#include <QKeySequence>
#include <searchdialog.h>
#include <QMessageBox>
#include <lineeditdelegate.h>
#include <QToolBar>
#include "filestatewatcher.h"

DataWindow::DataWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DataWindow)
{
    ui->setupUi(this);

    fileutil=NULL;
    model=NULL;

    buildToolBar();

//    ui->openFileAction->setShortcut(QKeySequence::Open);    //设置快捷键
//    ui->saveFileAction->setShortcut(QKeySequence::Save);
//    ui->closeFileAction->setShortcut(QKeySequence::Close);
//    ui->SearchAction->setShortcut(QKeySequence::Find);

//    connect(ui->openFileAction,SIGNAL(triggered(bool)),this,SLOT(openFile()));
//    connect(ui->saveFileAction,SIGNAL(triggered(bool)),this,SLOT(saveFile()));
//    connect(ui->SearchAction,SIGNAL(triggered(bool)),this,SLOT(search()));
//    connect(ui->closeFileAction,SIGNAL(triggered(bool)),this,SLOT(closeFileOpend()));

    //this->showMaximized();
    setCentralWidget(ui->tableView);

    stateLabel=new QLabel(this);
    stateLabel->setText("等待操作");
    ui->statusBar->addWidget(stateLabel);

}

DataWindow::~DataWindow()
{
    delete ui;
    delete fileutil;
    delete model;
}



void DataWindow::openFile(QFile *file){
    int width=ui->tableView->width();

    stateLabel->setText("表格正在处理数据中...");

    FileUtil *file1=fileutil;             //备份，以防打开文件失败
    UtilModel *model1=model;

    fileutil=new FileUtil();
    model=new UtilModel();


    bool tag=fileutil->openFile(file);
    if(!tag){   //打开文件失败
        fileutil=file1;
        model=model1;
        return;
    }
    model->setFileUtil(fileutil);            //设置model数据
    model->setFileData(fileutil->getContents());
    model->setHorizontalHeader(fileutil->getColumn_name());
    //model->printData();
    //model->printHeader();

   // QAbstractItemModel *oddmodel=ui->tableView->model();

    ui->tableView->setModel(model);            //设置tableview的model

   // delete oddmodel;

    LineEditDelegate *lineEditDelegate=new LineEditDelegate(this);
    ui->tableView->setItemDelegate(lineEditDelegate);

    stateLabel->setText("完成");
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setStretchLastSection(true);
    ui->tableView->resizeRowsToContents();
    ui->tableView->resizeColumnsToContents();
    ui->tableView->setEditTriggers(QAbstractItemView::DoubleClicked|QAbstractItemView::SelectedClicked|QAbstractItemView::EditKeyPressed);

    QHeaderView *header=ui->tableView->horizontalHeader();
    int count=header->count();
    if(this->isMaximized()){
        for(int i=0;i<count-1;++i){
            header->resizeSection(i,width/count);
        }
    }
    //header->setStyleSheet("QHeaderView::section{background-color:green;}");
    this->update(); //刷新
}

void DataWindow::saveFile(){

    if(fileutil==NULL){
        QMessageBox::information(this,"错误","没有打开有效文件！");
        return;
    }
//    if(!model->isModifyed()){  //没有修改，不用保存
//        return;
//    }
    if(FileStateWatcher::getInstance()->getFileEditState(fileutil->getFileFullName())==FileEditState::Saved){
        return;       //用状态机判断 ，文件没有修改，不用保存
    }

    fileutil->saveFile();
    model->setModifyTag(false);
}

void DataWindow::search(){
    if(fileutil==NULL){
        QMessageBox::information(this,"错误！","没有打开文件！");
        return ;
    }
    SearchDialog *searchDialog=new SearchDialog(this,ui->tableView,model);
    searchDialog->show();
}

void DataWindow::closeEvent(QCloseEvent *event){
    qDebug()<<"close event ++++++++++++++++++";
    QMainWindow::closeEvent(event);
    if(fileutil==NULL){
        return;
    }
    if(FileStateWatcher::getInstance()->getFileEditState(fileutil->getFileFullName())==FileEditState::UnSaved){
        int tag=QMessageBox::warning(this,"错误！","是否保存？",QMessageBox::Save|QMessageBox::Cancel);
        if(tag==QMessageBox::Save){
            fileutil->saveFile();
            model->setModifyTag(false);
            FileStateWatcher::getInstance()->setFileEditState(fileutil->getFileFullName(),false);     //设置修改已保存，文件为未编辑状态
        }
    }
}

void DataWindow::closeFileOpend(){
    if(fileutil!=NULL){
        delete fileutil;
        delete model;
        fileutil=NULL;     //置位NULL，不然下次打开会崩溃
        model=NULL;
    }
}
void DataWindow::buildToolBar(){      //建立工具栏和其工具栏按钮
    saveAction=new QAction(QIcon(":/img/img/save.png"),"保存");
    searchAction=new QAction(QIcon(":/img/img/search.png"),"查找");
    closeAction=new QAction(QIcon(":/img/img/close.png"),"关闭");
    connect(saveAction,SIGNAL(triggered(bool)),this,SLOT(saveFile()));
    connect(searchAction,SIGNAL(triggered(bool)),this,SLOT(search()));
    connect(closeAction,SIGNAL(triggered(bool)),this,SLOT(closeFileOpend()));
    commonToolBar=new QToolBar(this);
    commonToolBar->addAction(saveAction);  //添加action
    commonToolBar->addAction(searchAction);
    commonToolBar->addAction(closeAction);

    this->addToolBar(commonToolBar);
}
